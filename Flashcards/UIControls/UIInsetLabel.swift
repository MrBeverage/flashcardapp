//
//  UIInsetLabel.swift
//  Flashcards
//
//  Created by Alex Beverage on 13/05/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

import Foundation
import UIKit

class UIInsetLabel : UILabel {
    
    @IBInspectable var left : CGFloat = 0.0
    @IBInspectable var right : CGFloat = 0.0
    @IBInspectable var top : CGFloat = 0.0
    @IBInspectable var bottom : CGFloat = 0.0
    
    override func drawTextInRect(rect: CGRect) {
        let insets = UIEdgeInsets(top: top, left: left, bottom: bottom, right: right)
        super.drawTextInRect(UIEdgeInsetsInsetRect(rect, insets))
    }
    
    override func intrinsicContentSize() -> CGSize {
        var size = super.intrinsicContentSize();
        size.width  += self.left + self.right;
        size.height += self.top + self.bottom;
        return size;
    }
}