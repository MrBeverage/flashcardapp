//
//  DeckTableViewCell.swift
//  Flashcards
//
//  Created by Alex Beverage on 06/06/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

import UIKit

@IBDesignable
class DeckTableViewCell: UITableViewCell {

    @IBOutlet weak var deckNameField: UILabel!
    @IBOutlet weak var cardCountField: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
