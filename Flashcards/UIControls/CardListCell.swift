//
//  CardListCell.swift
//  Flashcards
//
//  Created by Alex Beverage on 8/14/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

import UIKit

class CardListCell: UITableViewCell {

    @IBOutlet private weak var questionField: UILabel!
    @IBOutlet private weak var answerField: UILabel!
    @IBOutlet private weak var scoreField: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setValues(question: String, answer: String, score: Int) {
        questionField?.text = question
        answerField?.text = answer
        scoreField?.text = "\(score)%"
        
        if score > 90 {
            scoreField?.textColor = UIColor.blueColor()
        } else if score > 80 {
            scoreField?.textColor = UIColor(red: 0.0, green: 170.0 / 256.0, blue: 0.0, alpha: 1.0)
        } else if score > 70 {
            scoreField?.textColor = UIColor(red: 173.0 / 256.0, green: 173.0 / 256.0, blue: 0.0, alpha: 1.0)
        } else if score > 50 {
            scoreField?.textColor = UIColor(red: 1.0, green: 0.5, blue: 0.0, alpha: 1.0)
        } else {
            scoreField?.textColor = UIColor.redColor()
        }
    }
}
