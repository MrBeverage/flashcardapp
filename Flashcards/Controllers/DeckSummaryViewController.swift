//
//  DetailViewController.swift
//  Flashcards
//
//  Created by Alex Beverage on 22/04/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

import UIKit
import CoreData
import JSQCoreDataKit

class DeckSummaryViewController: UIViewController, RequiresCoreData, RequiresDeck {

    @IBOutlet weak var deckNameLabel: UILabel!
    @IBOutlet weak var deckInfoLabel: UILabel!
    
    @IBOutlet weak var nextButton: UIButton!

    var deck: Deck? = nil {
        didSet {
            self.configureView()
        }
    }
    
    private var context: NSManagedObjectContext? = nil
    var stack: CoreDataStack? = nil {
        didSet {
            context = stack?.managedObjectContext
        }
    }

    func configureView() {
        
        //  Because "\(deck?.blah)" results in 'Optional' being printed:
        if deck != nil {
            deckNameLabel?.text = deck!.name
            deckInfoLabel?.text = "\(deck!.cards.count) cards"
        }
        
        nextButton?.enabled = deck?.cards.count > 0
        navigationItem.rightBarButtonItem?.enabled = deck?.locked == false
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureView()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        configureView()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "StartQuiz" {
            let controller = segue.destinationViewController as! AnswerController
            
            controller.stack = stack
            controller.deck = deck
            
            controller.cardSelector = SimpleSelector(deck: controller.deck!)
            controller.quizLength = 3
            
        } else if segue.identifier == "CreateCard" {
            let controller = segue.destinationViewController as! CreateCardController
            
            controller.stack = stack
            controller.deck  = deck
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func canPerformUnwindSegueAction(action: Selector, fromViewController: UIViewController, withSender sender: AnyObject) -> Bool {
        
        return action == "unwindToDeckSummary:"
    }

    @IBAction func unwindToDeckSummary(segue: UIStoryboardSegue) {

    }
}

