//
//  DeckActionsViewController.swift
//  Flashcards
//
//  Created by Alex Beverage on 7/27/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

import UIKit
import CoreData
import JSQCoreDataKit

class DeckActionsTabBarController: UITabBarController, UITabBarControllerDelegate, RequiresCoreData {

    var deck: Deck? = nil {
        didSet {
            configureTabs()
        }
    }
    
    private var context: NSManagedObjectContext? = nil
    var stack: CoreDataStack? = nil {
        didSet {
            context = stack?.managedObjectContext
        }
    }
    
    private func configureTabs() {
        for controller in self.viewControllers! {
            if let viewController = controller as? UIViewController {
                setFieldsForProtocols(viewController)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureTabs()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        for controller in (viewControllers!.filter { $0 as? CreateCardController != nil }) {
            controller.tabBarItem?.enabled = !deck!.locked
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    

    private func setFieldsForProtocols(viewController: UIViewController) {
        
        var targetController: UIViewController
        
        if let navigationController = viewController as? UINavigationController {
            targetController = navigationController.topViewController
        } else {
            //  There actually may be more cases to check here:
            targetController = viewController
        }
        
        if let coreDataController = targetController as? RequiresCoreData {
            coreDataController.stack = stack
        }
        
        if let deckController = targetController as? RequiresDeck {
            deckController.deck = deck
        }
    }
    
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        
        //  This is the top view on the navigation controller's stack.  If we want any
        //  navigation bar items from child views to appear on the nav bar, we need to
        //  add and remove them here.
        
        //  This should be a callback from the create card controller if a new one is saved.
        //  For now reload the deck every time we switch to the card list view:
        //deck = Deck.fetch(context!, shortName: deck!.shortName)
        
        //  There could be many more nav bar items we'll care about later:
        navigationItem.rightBarButtonItem = viewController.navigationItem.rightBarButtonItem
    }
}
