//
//  QuizViewController.swift
//  Flashcards
//
//  Created by Alex Beverage on 26/04/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

import AVFoundation
import Foundation
import UIKit
import CoreData
import JSQCoreDataKit

//
//  Class that will manage the selection and presentation of cards when the app
//  is in quiz-mode.
//
class AnswerController : UIViewController, CardAudioHandler, CardResultHandler, CardActionHandler, RequiresCoreData, RequiresDeck {
    
    private var context: NSManagedObjectContext? = nil
    var stack: CoreDataStack? = nil {
        didSet {
            context = stack?.managedObjectContext
        }
    }
    
    @IBOutlet weak var cardView: AnswerCardViewClass!
    @IBOutlet weak var responseView: AnswerResponseViewClass!
    @IBOutlet weak var actionView: ChallengeActionView!
    
    @IBAction func swipeRight(sender: AnyObject) {
        cardResult(CardResult.Fail)
    }
    
    private let audioService = AudioService()
    
    //  Quiz state:
    var deck: Deck? = nil
    var cardSelector: CardSelector? = nil
    var currentCard: Card? = nil
    var quizLength = 0
    var counter = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.cardView.audioHandler = self
        self.responseView.resultHandler = self
        self.actionView.actionHandler = self
        
        updateCard()
        setChallengeMode()
    }
    
    func updateCard() {
        if counter < quizLength && counter < cardSelector?.cards.count {
            currentCard = cardSelector!.nextCard()
            cardView.cardCharacters.text = currentCard!.content.traditional
            cardView.cardDefinition.text = currentCard!.content.definitions
            cardView.cardTransliteration.updateTransliterationText(currentCard!.content.pinyin)
            counter++
        } else {
            //  Segue to a quiz summary scene:
            performSegueWithIdentifier("QuizSummary", sender: self)
        }
    }
    
    func playCardAudio() {
        //  The view contoller's card could change out from under us, or be destroyed.
        let card = currentCard!
        
        let audioCompleted = cardView.cardTransliteration.audioPlaybackCompleted
        let saveCompleted: (ContextSaveResult) -> () = { result in
            if result.success {
                NSLog("Saved audio for \(card.content.traditional)")
            } else {
                NSLog("Unable to save audio for \(card.content.traditional): \(result.error)")
            }
        }
        
        if card.content.audio != nil {
            audioService.playAudio(currentCard!.content.audio!, completionHandler: audioCompleted)
        } else {
            //  There is no audio yet saved for the card.  Two cases here:
            //   1. The card is only a single character - take the audio from any saved Hanzi object if present.
            //   2. The card is a longer word - download audio for the entire phrase.
            if Swift.count(card.content.traditional) == 1 {
                if let audio = Hanzi.fetch(context!, traditional: card.content.traditional)?.audio {
                    card.content.audio = audio
                }
            }
            
            audioService.getCardAudio(context!, cardContent: card.content) { audio, error in
                card.content.audio = audio
                saveContext(self.context!, saveCompleted)
                self.audioService.playAudio(audio, completionHandler: audioCompleted)
            }
        }
        
    }
    
    func cardResult(result: CardResult) {
        setChallengeMode()
        updateCard()
    }
    
    func cardAction(cardAction: CardAction) {
        //  The only 'action' so far is flip.
        //  May add more such as hint, or play audio.
        switch cardAction {
        case .Flip:
            setAnswerMode()
            break
        }
    }

    private func setChallengeMode() {
        actionView.hidden = false
        responseView.hidden = true
        cardView.showFront()
    }
    
    private func setAnswerMode() {
        actionView.hidden = true
        responseView.hidden = false
        
        currentCard?.timesViewed++
        //  Scoring happens here.
        
        //  Async-ify this:
        saveContextAndWait(context!)
        
        cardView.showBack()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "QuizSummary" {
            if let controller = segue.destinationViewController as? QuizSummaryController {
                //  Things to eventually initialize here:
                //      1. Card result summary
                //      2. Changes to deck statistics
                //
            }
        }
    }
    
    override func canPerformUnwindSegueAction(action: Selector, fromViewController: UIViewController, withSender sender: AnyObject) -> Bool {
        
        return action == "continueQuiz:"
    }
    
    @IBAction func continueQuiz(segue: UIStoryboardSegue) {
    
    }
}