//
//  MasterViewController.swift
//  Flashcards
//
//  Created by Alex Beverage on 22/04/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

import UIKit
import CoreData
import JSQCoreDataKit

class DeckListViewController: UITableViewController, RequiresCoreData { //, NSFetchedResultsControllerDelegate {

    @IBOutlet var deckListView: UITableView!
    
    var detailViewController: DeckSummaryViewController? = nil
    var dataSource: DeckDataSource!
    
    private var context: NSManagedObjectContext? = nil
    var stack: CoreDataStack? = nil {
        didSet {
            context = stack?.managedObjectContext
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        if UIDevice.currentDevice().userInterfaceIdiom == .Pad {
            self.clearsSelectionOnViewWillAppear = false
            self.preferredContentSize = CGSize(width: 320.0, height: 600.0)
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        tableView.reloadData()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //deckListView.registerNib(UINib(nibName: "DeckTableViewCell", bundle: nil), forCellReuseIdentifier: "DeckCell")
        
        dataSource = tableView.dataSource! as! DeckDataSource
        
        dataSource.stack = stack
        dataSource.deckCellConfigurationBlock = { (cell: DeckTableViewCell, indexPath: NSIndexPath, deck: Deck) -> () in
            cell.deckNameField!.text = deck.name
            cell.cardCountField!.text = "\(deck.cards.count) card"
            
            if deck.cards.count != 1 {
                cell.cardCountField!.text = "\(cell.cardCountField!.text!)s"
            }
        }
        
        if let split = self.splitViewController {
            let controllers = split.viewControllers
            self.detailViewController = controllers[controllers.count-1].topViewController as? DeckSummaryViewController
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*

    func insertNewObject(sender: AnyObject) {
        let context = self.fetchedResultsController.managedObjectContext
        let entity = self.fetchedResultsController.fetchRequest.entity!
        
        //  Add new lists here to the custom list category, or official lists to the static category.
        let newManagedObject = NSEntityDescription.insertNewObjectForEntityForName(entity.name!, inManagedObjectContext: context) as! NSManagedObject
             
        // If appropriate, configure the new managed object.
        // Normally you should use accessor methods, but using KVC here avoids the need to add a custom class to the template.
        newManagedObject.setValue(NSDate(), forKey: "timeStamp")
             
        // Save the context.
        var error: NSError? = nil
        if !context.save(&error) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            //println("Unresolved error \(error), \(error.userInfo)")
            abort()
        }
    }
*/
    
    // MARK: - Segues

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "DeckDetails" {

            if let dest = segue.destinationViewController as? DeckActionsTabBarController {
                
                let indexPath = tableView.indexPathForSelectedRow()
                let sectionName = dataSource.decks.keys.array[indexPath!.section]
                let deck = dataSource.decks[sectionName]?[indexPath!.row]
                
                dest.stack = stack
                dest.deck  = deck
                dest.title = deck?.name
                
                dest.delegate = dest
            }
        } else if segue.identifier == "CreateDeck" {
            let controller = segue.destinationViewController as! CreateDeckViewController
            controller.deckDataSource = self.dataSource
        }
    }

    // MARK: - Fetched results controller

        /*
    var fetchedResultsController: NSFetchedResultsController {
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let fetchRequest = NSFetchRequest()
        // Edit the entity name as appropriate.
        let entity = NSEntityDescription.entityForName("Event", inManagedObjectContext: self.managedObjectContext!)
        fetchRequest.entity = entity
        
        // Set the batch size to a suitable number.
        fetchRequest.fetchBatchSize = 20
        
        // Edit the sort key as appropriate.
        let sortDescriptor = NSSortDescriptor(key: "timeStamp", ascending: false)
        let sortDescriptors = [sortDescriptor]
        
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        // Edit the section name key path and cache name if appropriate.
        // nil for section name key path means "no sections".
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.managedObjectContext!, sectionNameKeyPath: nil, cacheName: "Master")
        aFetchedResultsController.delegate = self
        _fetchedResultsController = aFetchedResultsController
        
    	var error: NSError? = nil
    	if !_fetchedResultsController!.performFetch(&error) {
    	     // Replace this implementation with code to handle the error appropriately.
    	     // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
             //println("Unresolved error \(error), \(error.userInfo)")
    	     abort()
    	}
        
        return _fetchedResultsController!
    }    
    var _fetchedResultsController: NSFetchedResultsController? = nil

    
    func controllerWillChangeContent(controller: NSFetchedResultsController) {
        self.tableView.beginUpdates()
    }

    func controller(controller: NSFetchedResultsController,
            didChangeSection sectionInfo: NSFetchedResultsSectionInfo,
            atIndex          sectionIndex: Int,
            forChangeType    type: NSFetchedResultsChangeType) {
                
        switch type {
            case .Insert:
                self.tableView.insertSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Fade)
            case .Delete:
                self.tableView.deleteSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Fade)
            default:
                return
        }
    }

    func controller(controller: NSFetchedResultsController,
            didChangeObject anObject: AnyObject,
            atIndexPath     indexPath: NSIndexPath?,
            forChangeType   type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
                
        switch type {
            case .Insert:
                tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: .Fade)
            case .Delete:
                tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: .Fade)
            case .Update:
                self.configureCell(tableView.cellForRowAtIndexPath(indexPath!)!, atIndexPath: indexPath!)
            case .Move:
                tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: .Fade)
                tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: .Fade)
            default:
                return
        }
    }

    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        self.tableView.endUpdates()
    } */

    /*
     // Implementing the above methods to update the table view in response to individual changes may have performance implications if a large number of changes are made simultaneously. If this proves to be an issue, you can instead just implement controllerDidChangeContent: which notifies the delegate that all section and object changes have been processed.
     
     func controllerDidChangeContent(controller: NSFetchedResultsController) {
         // In the simplest, most efficient, case, reload the table view.
         self.tableView.reloadData()
     }
     */
}

