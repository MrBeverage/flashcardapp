//
//  CreateCardController.swift
//  Flashcards
//
//  Created by Alex Beverage on 19/05/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

import UIKit
import CoreData
import JSQCoreDataKit
import BrightFutures

class CreateCardController: UIViewController, CardAudioHandler, RequiresCoreData, RequiresDeck {

    private var context: NSManagedObjectContext? = nil
    private var cardContext: NSManagedObjectContext? = nil
    var stack: CoreDataStack? = nil {
        didSet {
            //  Only make a change if the context has actually change.
            if  stack?.managedObjectContext != context {
                //  Always use a child context on this page for easy discarding of changes:
                context = stack?.managedObjectContext
            
                //  I should probably either reset any existing child context, or merge any
                //  existing changes.  This should never really be set while the view is being
                //  actively used - only on initialization from the parent view's seque handler.
                //  It would be nice if there was a way to enforce that.
                cardContext = stack?.childManagedObjectContext(concurrencyType: stack!.managedObjectContext.concurrencyType, mergePolicyType: NSMergePolicyType.OverwriteMergePolicyType)
                
                //  Wipe any pending card.  Can make this better later:
                clearCard()
            }
        }
    }
    
    var deck: Deck?
    
    //  Intermediate data:
    private var childContextDeck: Deck?
    private var content: CardContent?
    private var currentTransliteration: [TransliterationItem] = []
    
    //  For tracking keyboard height.  This is so various UI element's delegates can shift
    //  the screen accordingly when selected.  Different controls may want to shift upwards
    //  differently.  Figure out why I can't just use a scroll view later:
    private var currentScreenShiftValue: CGFloat = 0
    
    //  Services:
    private lazy var audioService = AudioService()
    private lazy var translateService = TranslateService()
    
    //  UI:
    @IBOutlet weak var characterOutput: UILabel!
    @IBOutlet weak var transliterationView: TransliterationView!
    @IBOutlet weak var inputField: UITextField!
    @IBOutlet weak var definitionInputView: DefinitionInputView!
    
    @IBOutlet weak var downloadingTransliterations: UIActivityIndicatorView!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        automaticallyAdjustsScrollViewInsets = false
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

        configureView()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillShow:", name: UIKeyboardWillChangeFrameNotification, object: nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    private func configureView() {
        
        inputField?.delegate = self
        definitionInputView?.textInput.delegate = self
        transliterationView?.audioHandler = self
        
        //  Re-fetch the deck in our child context:
        if content == nil {
            childContextDeck = Deck.fetch(cardContext!, shortName: deck!.shortName)!
            content = CardContent(context: cardContext!, traditional: "", simplified: "", pinyin: "", definitions: nil, audio: nil)
        }
        
        updateDisplayStrings(currentTransliteration)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func saveButtonPressed(sender: AnyObject) {
        //  In order to reach here, all character information must have been downloaded.
        //  Traditional, pinyin, deck, and audio will all have already been set:
        content!.simplified = buildSimplified(characterOutput.text!)
        
        if definitionInputView.isTextMode() {
            content!.definitions = definitionInputView.getDefinitionText()
        } else if definitionInputView.isImageMode() {
            //  Save image here.
        }   //  Else do nothing.
        
        NSLog("Adding card: \(content)")
        
        let emptyScoreData = CardScoreData(context: cardContext!)
        
        let card = Card(context: cardContext!, questionType: .Text, answerType: .Text, dateAdded: NSDate(), timesViewed: 0, content: content!, scoreData: emptyScoreData, deck: childContextDeck!)
        
        saveContext(cardContext!) { success, error in
            if success {
                //  Tell the parent tab controller to refresh its deck:
                let deckActionsController = self.parentViewController as! DeckActionsTabBarController
                deckActionsController.deck = Deck.fetch(self.context!, shortName: self.deck!.shortName)
                
                //  In the background, go and fill in any missing Hanzi audio:
                self.audioService.downloadAndSaveMissingAudioForCardCharacters(self.context!, text: self.content!.traditional)
                
                self.clearCard()
                
                //  TODO: Put some sort of saved message up:
            }
        }
    }
    
    private func buildSimplified(traditional: String) -> String {
        var simplified = ""
        
        for c in traditional {
            let character = String(c)
            if let h = Hanzi.fetch(context!, traditional: character) {
                simplified += h.simplified
            } else {
                simplified += character
            }
        }
        
        return simplified
    }
    
    @IBAction func inputChanged(sender: AnyObject) {
        
        if inputField.text!.isEmpty {
            navigationItem.rightBarButtonItem?.enabled = false
            currentTransliteration = []
            transliterationView.updateTransliterationText("")
            characterOutput.text = ""
            
            //  Blank out the three card values that are being constantly updated:
            content!.traditional = ""
            content!.pinyin = ""
            content!.audio = nil
        } else {
            let transliterableInput = getTransliterableText(inputField)
            currentTransliteration  = buildTransliterationItems(transliterableInput)
            updateDisplayStrings(currentTransliteration)
        }
    }

    override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
        
        //  Multi-touch is disabled by default - we can assume only one touch event per call for now.
        if let touch = touches.first as? UITouch {

            if definitionInputView.textInput.isFirstResponder() && touch.view != definitionInputView.textInput {
                
                definitionInputView.textInput.resignFirstResponder()
            } else if inputField.isFirstResponder() && touch.view != inputField {
                inputField.resignFirstResponder()
            }
        }
        super.touchesEnded(touches, withEvent: event)
    }
    
    //  MARK: Transliteration
    class TransliterationItem {
        
        let text: String
        var transliteration: String = ""
        
        init(text: String) {
            self.text = text
        }
    }
    
    func getTransliterableText(textField: UITextField) -> String {
        var transliteratable = textField.text
        
        if textField.markedTextRange != nil {
            let start = transliteratable.startIndex
            let endIndex = textField.offsetFromPosition(textField.beginningOfDocument, toPosition: textField.markedTextRange!.start)
            
            transliteratable = transliteratable.substringWithRange(
                Range<String.Index>(start: start, end: advance(start, endIndex)))
        }
        
        return transliteratable
    }
    
    func updateDisplayStrings(transliteration: [TransliterationItem]) {
        
        let displayStrings = buildDisplayStrings(transliteration)
        characterOutput?.text = displayStrings.display
        transliterationView?.updateTransliterationText(displayStrings.transliteration)
        
        //  Update the intermediate card:
        content?.traditional = displayStrings.display
        content?.pinyin = displayStrings.transliteration
        
        if isTransliterated(currentTransliteration) {
            navigationItem.rightBarButtonItem?.enabled = true
        }
    }
    
    func buildDisplayStrings(items: [TransliterationItem]) -> (display: String, transliteration: String) {
        //  Later this should return attributed text.
        var displayOutput = ""
        var transliterationOutput = ""
        var previousItemWasCharacter = false
        
        for item in items {
            if item.text.isEmpty == false {
                displayOutput += item.text
                
                if StringUtils.isChineseCharacter(item.text[item.text.startIndex]) {
                    if item.transliteration.isEmpty {
                        //  Output the character as-is.  A background process is looking up the 
                        //  pinyin and will update the field later.
                        transliterationOutput += item.text
                        previousItemWasCharacter = true
                    } else {
                        if previousItemWasCharacter {
                            transliterationOutput += " "
                        }
                        
                        transliterationOutput += item.transliteration + " "
                        previousItemWasCharacter = false
                    }
                } else {
                    if previousItemWasCharacter {
                        transliterationOutput += " "
                    }
                    
                    transliterationOutput += item.text + " "
                    previousItemWasCharacter = false
                }
            }
        }
        
        return (display: displayOutput, transliteration: transliterationOutput)
    }
    
    func buildTransliterationItems(text: String) -> [TransliterationItem] {
        
        var newTransliteration: [TransliterationItem] = []
        
        //  For building non-Chinese words/characters.  Examples could include things like 牛B.
        var currentWord = ""
        
        for c in text {
            
            var character = String(c)
            
            //  If it is a Chinese character, append a single-character TransliterationItem
            if StringUtils.isChineseCharacter(c) {
                
                //  If a non-Chinese word was being built, finish it:
                if currentWord.isEmpty == false {
                    newTransliteration.append(TransliterationItem(text: currentWord))
                    currentWord = ""
                }
                
                var newItem = TransliterationItem(text: String(c))
                
                //  Lookup the character:
                let hanzi = Hanzi.fetch(context!, traditional: character)
                
                if hanzi?.transliteration.isEmpty == false {
                    newItem.transliteration = hanzi!.transliteration
                } else {
                    navigationItem.rightBarButtonItem?.enabled = false
                    
                    transliterationView.alpha = 0.5
                    transliterationView.addSubview(downloadingTransliterations)
                    downloadingTransliterations.bringSubviewToFront(transliterationView)
                    downloadingTransliterations.startAnimating()
                    
                    //  Lookup pinyin for this character.
                    translateService.lookupTraditional(character, responseHandler: characterLookupHandler)
                }
                
                newTransliteration.append(newItem)
            } else {
                currentWord += character
            }
        }
        
        if currentWord.isEmpty == false {
            newTransliteration.append(TransliterationItem(text: currentWord))
        }
        
        return newTransliteration
    }
    
    private func characterLookupHandler(traditional: String, simplified: String?, pinyin: String?, error: NSError?) {
        if error == nil {
            var h = Hanzi.fetch(self.context!, traditional: traditional)
            
            if h == nil {
                NSLog("Adding new character \(traditional) - \(pinyin)")
                h = Hanzi(context: self.context!, traditional: traditional, simplified: simplified!, transliteration: pinyin!, audio: nil)
            } else {
                NSLog("Updating character \(traditional) - \(pinyin)")
                h?.simplified = simplified!
                h?.transliteration = pinyin!
            }
            
            //  Find out if no changes will results in a write to the store.
            saveContext(self.context!) { result in
                if !result.success {
                    //  Handle this error too.
                }
            }
            
            updateTransliterationItems(traditional, transliteration: pinyin!)
            
        } else {
            //  Handle the error
        }
    }
    
    private func updateTransliterationItems(character: String, transliteration: String) {
        
        var changed = false
        for item in (currentTransliteration.filter { $0.text == character }) {
            item.transliteration = transliteration
            changed = true
        }
        
        if changed {
            updateDisplayStrings(currentTransliteration)
        }
        
        if isTransliterated(currentTransliteration) {
            transliterationView.alpha = 1
            downloadingTransliterations.stopAnimating()
            navigationItem.rightBarButtonItem?.enabled = true
        } else {
            navigationItem.rightBarButtonItem?.enabled = false
        }
    }
    
    private func isTransliterated(items: [TransliterationItem]) -> Bool {
        
        if items.count == 0 {
            return false
        }
        
        for item in items {
            if count(item.text) == 1 && item.transliteration.isEmpty && StringUtils.isChineseCharacter(item.text[item.text.startIndex]) {
                return false
            }
        }
        return true
    }
    
    //  MARK: - Audio
    func playCardAudio() {
        audioService.getCardAudio(context!, cardContent: content!) { audio, error in
            self.audioService.playAudio(audio, completionHandler: self.transliterationView.audioPlaybackCompleted)
        }
    }
    
    private func clearCard() {
        characterOutput?.text = ""
        transliterationView?.updateTransliterationText("")
        inputField?.text = ""
        
        definitionInputView?.reset()
        
        currentTransliteration = []
        downloadingTransliterations?.stopAnimating()
        saveButton?.enabled = false
        
        childContextDeck = nil
        content = nil
    }
    
    func keyboardWillShow(notification: NSNotification) {
        
        if let start = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue(),
             let end = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey]   as? NSValue)?.CGRectValue() {
        
            let tabBarHeight = tabBarController?.tabBar.frame.height
            let shift = -(start.origin.y - end.origin.y) + (tabBarHeight ?? 0)
                
            currentScreenShiftValue = shift < 0 ? shift : 0
        }
    }
}

extension CreateCardController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(textField: UITextField) {
        view.scrollToView(inputField)
    }
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        navigationItem.rightBarButtonItem?.enabled = textField.text.isEmpty == false
        view.scrollToY(0)
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
}

extension CreateCardController: UITextViewDelegate {
    
    func textViewDidBeginEditing(textView: UITextView) {
        view.scrollToY(currentScreenShiftValue)
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        view.scrollToY(currentScreenShiftValue)
    }
}
