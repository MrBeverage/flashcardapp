//
//  CreateDeckController.swift
//  Flashcards
//
//  Created by Alex Beverage on 11/05/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

import Foundation
import UIKit

class CreateDeckViewController : UIViewController {
    
    var deckDataSource : DeckDataSource?
    
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var descriptionField: UITextView!
    
    @IBAction func addDeck(sender: AnyObject) {
        var newDeckName = nameField.text
        var newDeckDescription = descriptionField.text
        
        // TODO: error handling:
        deckDataSource?.createNewDeck(newDeckName!, description: newDeckDescription!)
        
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
}