//
//  DeckContentsViewController.swift
//  Flashcards
//
//  Created by Alex Beverage on 8/13/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

import UIKit
import CoreData
import JSQCoreDataKit

class CardListViewController : UITableViewController, RequiresCoreData, RequiresDeck {

    var dataSource: CardDataSource?
    var deck: Deck? = nil {
        didSet {
            dataSource?.deck = deck
        }
    }
    
    private var context: NSManagedObjectContext? = nil
    var stack: CoreDataStack? = nil {
        didSet {
            context = stack?.managedObjectContext
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let cellNib = UINib(nibName: "CardListCell", bundle: nil)
        tableView.registerNib(cellNib, forCellReuseIdentifier: CardDataSource.TableCellName)
        
        dataSource = tableView.dataSource as? CardDataSource
        
        dataSource?.deck = deck
        dataSource?.stack = stack
        
        dataSource?.cardCellConfigurationBlock = { (cell: CardListCell, indexPath: NSIndexPath, card: Card?) -> () in
            
            //  Set a random score for now:
            let randomScore = Int(arc4random_uniform(10)) * 5 + 50
            cell.setValues(card?.content.traditional ?? "", answer: card?.content.pinyin ?? "", score: randomScore)
        }
        
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
