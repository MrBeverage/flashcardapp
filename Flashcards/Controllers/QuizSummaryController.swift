//
//  QuizSummaryController.swift
//  Flashcards
//
//  Created by Alex Beverage on 04/06/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

import Foundation
import UIKit

class QuizSummaryController: UIViewController {

    override func viewDidLoad() {
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ContinueQuiz" {
            if let quizController = segue.destinationViewController as? AnswerController {
                quizController.counter = 0
                quizController.updateCard()
            }
        }
    }
}
