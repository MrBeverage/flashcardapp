#!/usr/bin/perl -w

# Script to convert the raw HSK data files from http://data.hskhsk.com/lists/ into the format
# we need in the flashcard app.
#
# I had never written perl scripts before I started this app.  I'm likely doing a million
# things wrong, or in non-optimal ways.
#

use strict;
use warnings;

# We're dealing with UTF-8
use open IO => ':encoding(utf8)';
binmode(STDOUT, ":utf8");

sub insertXintoYatZ{
    my ( $X , $Y , $Z ) = @_;
    substr( $Y , $Z , -length($Y) ) = $X ;
    return $Y;
}

sub trim {
  my $s = shift;
  $s =~ s/^\s+|\s+$//g;
  return $s
};

my $hskInput = $ARGV[0];

open(INFILE, $hskInput) || die $!;
my @lines = <INFILE>;
close INFILE;

open OUTFILE, ">", "./output.json" || die $!;

print OUTFILE "\"cards\" : [\n";

foreach my $line (@lines) {
  # HSK file schema: simplified traditional pinyin# pinyin definitions
  # Both pinyins are not required - we can keep the numbered pinyin as we
  # already have utilities for converting them for display.
  my @elements = split /[\s ]/, $line;

  # There can be multiple transliterations for some items:
  # 要	要	yao4, yao1	yào, yāo	to want; to need; will/shall; important | demand; request
  # After extracting characters, seek a comma separator - if not is found then there is
  # only a single transliteration.
  #
  # In the end, we may want both kinds of transliteration - tone numbers would be quite
  # easy and fast indexes into the audio tables later.
  my $simplified  = $elements[0];
  my $traditional = $elements[1];

  # Find the start index of the pinyin-with-tone-marks list.  It is located at index
  # 2 + #transliterations.  They are comma-separated.
  my $num_pinyins = 1;
  for ($_ = $elements[1 + $num_pinyins] ; m/.*,/; $_ = $elements[2 + $num_pinyins], $num_pinyins++) { }

  # $start_index now points the first tone-marked transliteration.  The last one is
  # 2 * $start_index - 2
  my @pinyin_tones = ();
  my @pinyin_numbers = ();
  for (my $i = 0; $i < $num_pinyins; $i++) {
    my $number_result = $elements[2 + $i];
    my $tone_result = $elements[2 + $num_pinyins + $i];

    $number_result =~ s/,//;
    $tone_result =~ s/,//;

    # Need to split the tone-marked pinyin strings based on where the tone numbers
    # are located in the tone-number strings.
    # To strlen-1 because we don't care about the final tone.
    for (my $j = 0; $j < (length $number_result) - 1; $j++) {
      my $char = substr($number_result, $j, 1);
      if ($char =~ /\d/) {
        $tone_result = insertXintoYatZ(" ", $tone_result, $j);
      }
    }

    push(@pinyin_tones, $tone_result);
  }

  # Combine individual definitions.  A definition here is a sequence of words terminated
  # by either a ; or a |.
  my @definitions = ();
  my $builder = "";
  foreach my $element (@elements[(2 + 2 * $num_pinyins)..$#elements]) {
    $builder = $builder." ".$element;
    if ($element =~ /;|\|/) {
      $builder =~ s/[;|]//;
      $builder = trim($builder);
      if (length $builder > 0) {
        push(@definitions, $builder);
      }
      $builder = "";
    }
  }
  push(@definitions, trim($builder));

  print OUTFILE "\t{\n";
  print OUTFILE "\t\t\"simplified\"  : \"".$simplified."\",\n";
  print OUTFILE "\t\t\"traditional\" : \"".$traditional."\",\n";
  print OUTFILE "\t\t\"pinyin\"      : \[ ";
  foreach my $pinyin (@pinyin_tones) {
    print OUTFILE "\"".$pinyin."\", ";
  }
  print OUTFILE "],\n";
  print OUTFILE "\t\t\"definitions\" : \[ ";
  foreach my $definition (@definitions) {
    my $result = $definition;
    $definition =~ s/[;|]//;
    print OUTFILE "\"".$result."\", ";
  }
  print OUTFILE "\]\n";
  print OUTFILE "\t},\n";
}

print OUTFILE "]\n";

close OUTFILE;
