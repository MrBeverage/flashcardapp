#!/usr/bin/perl -w

use strict;
use warnings;

use utf8;

package ToneSet;

use Data::Printer;

# new(base_tone, tone_array) -> this
sub new {
  my $class = shift;

  #my ($base_tone, @tone_array) = @_;
  my $base_tone  = shift;
  my $tone_array = shift;

  my %tone_table;
  for my $tone (@{$tone_array}) {
    $tone_table{$tone} = "";
  }

  my $self = {
    base  => $base_tone,
    tones => { %tone_table }
  };

  bless $self, $class;

  return $self;
}

sub acceptsTone {
  my $self = shift;
  my $tone = shift;

  return defined $self->{tones}{$tone};
}

# hasTone(tone) -> bool
sub hasTone {
  my $self = shift;
  my $tone = shift;

  return (length $self->{tones}{$tone});
}

# isFilled() -> bool
sub isFilled {
  my $self = shift;

  foreach my $key (keys $self->{tones}) {
    if (length $self->{tones}{$key} == 0) {
      return 0;
    }
  }

  return 1;
}

# toString() -> string
sub toString {
  my $self = shift;

  my $string = $self->{base}.": ";

  foreach my $tone (keys $self->{tones}) {
    $string = $tone." -> ".$string.$self->{tones}{$tone}.", ";
  }

  return $string;
}

return 1;
