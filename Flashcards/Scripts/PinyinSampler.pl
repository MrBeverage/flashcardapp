#!/usr/bin/perl -w

# Script for acquiring pinyin samples for all Mandarin tones.

# Copy this to Google downloader script:
# Script for downloading all Mandarin tones individually from Google Translate.
# This may be a violation of their terms of service, but I can't figure out from
# what they've posted.  Figure that out before actually posting this app.

use strict;
use warnings;
use HTML::TokeParser::Simple;
use HTML::TreeBuilder::XPath;
use Mojo::DOM;
use Data::Printer;
use ToneSet;

#use encoding 'utf8', Filter => 1;
use utf8;
use open IO => ':encoding(utf8)';
binmode STDIN,  ':encoding(UTF-8)';
binmode STDOUT, ':encoding(UTF-8)';

sub charAt {
    return substr($0, $1, 1);
}

# Method for expanding neutral-tone pinyin into the four other tones:
# The pattern is that the final vowel that is not the last character in the
# pinyin recieves the tone marker unless the character is size 1.  I think.
# 'v' is a special vowel symbol for pinyin - for example, 女 = nv, not nu.
my %vowels;
foreach ( 'a', 'e', 'i', 'o', 'u', 'v') { $vowels{$_} = 1; }

my %tone_markers =
  ( 'a'  => [ 'ā', 'á', 'ǎ', 'à', 'a' ],
    'e'  => [ 'ē', 'é', 'ě', 'è', 'e' ],
    'i'  => [ 'ī', 'í', 'ǐ', 'ì', 'i' ],
    'o'  => [ 'ō', 'ó', 'ǒ', 'ò', 'o' ],
    'u'  => [ 'ū', 'ú', 'ǔ', 'ù', 'u' ],
    'v'  => [ 'ǘ', 'ǚ', 'ǜ' ],
    've' => [ 'üè' ]);

# getToneArray(base, target)
sub getToneArray {
  my $base = shift;
  my $target = shift;

  my @results = ();
  foreach my $tone (@{$tone_markers{$target}}) {
    my $temp = $base;
    $temp =~ s/$target/$tone/;
    push(@results, $temp);
  }
  return @results;
}

# expandPinyin(base)
sub expandPinyin {
    my $base = shift;

    # Need to special case the 've' tone.
    if ($base =~ 've') {
      return getToneArray($base, 've');
    }

    # If the base is larger than one character, the tone mark will not
    # go on the last character unless the last character is the only
    # vowel.
    my $last_character;

    for (my $i = (length $base) - 1; $i >= 0; $i--) {

        my $char = substr($base, $i, 1);

        if (length $base > 1 && $i == (length $base) - 1) {
            $last_character = $char;
            next;
        }

        if (exists $vowels{$char}) {
          return getToneArray($base, $char);
        }
    }

    return getToneArray($base, $last_character);
}

# buildGoogleTranslateRequest(characters)
sub buildGoogleTranslateRequest {
  my $baseQuery = "http://translate.google.com/#zh-CN/en/";
  my @characters = @_;

  my $query = "";
  foreach my $char (@characters) {
    $query = $query.$char.",";
  }
  return $baseQuery.$query;
}

# Base tones for all possible sounds in Mandarin Chinese
my @pinyin_bases = (
#'a' => [ '吖', '嗄', '', ''],
    'a', 'ai', 'an', 'ang', 'ao',

    'ba', 'bai', 'ban', 'bang', 'bao', 'bei', 'ben', 'beng',
    'bi', 'bian', 'biao', 'bie', 'bin', 'bing', 'bo', 'bu',

    'ca', 'cai', 'can', 'cang', 'cao', 'ce', 'cei', 'cen', 'ceng',
    'cha', 'chai', 'chan', 'chang', 'chao', 'che', 'chen', 'cheng',
    'chi', 'chong', 'chou', 'chu', 'chua', 'chuai', 'chuan', 'chuang',
    'chui', 'chun', 'chuo', 'ci', 'cong', 'cou', 'cu', 'cuan', 'cui',
    'cun', 'cuo',

    'da', 'dai', 'dan', 'dang', 'dao', 'de', 'den', 'dei', 'deng',
    'di', 'dia', 'dian', 'diao', 'die', 'ding', 'diu', 'dong', 'dou',
    'du', 'duan', 'dui', 'dun', 'duo',

    'e', 'ei', 'en', 'eng', 'er',

    'fa', 'fan', 'fang', 'fei', 'fen', 'feng', 'fo', 'fou', 'fu',

    'ga', 'gai', 'gan', 'gang', 'gao', 'ge', 'gei', 'gen', 'geng',
    'gong', 'gou', 'gu', 'gua', 'guai', 'guan', 'guang', 'gui',
    'gun', 'guo',

    'ha', 'hai', 'han', 'hang', 'hao', 'he', 'hei', 'hen', 'heng', 'hong',
    'hou', 'hu', 'hua', 'huai', 'huan', 'huang', 'hui', 'hun', 'huo',

    'ji', 'jia', 'jian', 'jiang', 'jiao', 'jie', 'jin', 'jing', 'jiong',
    'jiu', 'ju', 'juan', 'jue', 'jun',

    'ka', 'kai', 'kan', 'kang', 'kao', 'ke', 'ken', 'keng', 'kong', 'kou',
    'ku', 'kua', 'kuai', 'kuan', 'kuang', 'kui', 'kun', 'kuo',

    'la', 'lai', 'lan', 'lang', 'lao', 'le', 'lei', 'leng', 'li', 'lia',
    'lian', 'liang', 'liao', 'lie', 'lin', 'ling', 'liu', 'long', 'lou',
    'lu', 'lv', 'luan', 'lve', 'lun', 'luo',

    #'m',
    'ma', 'mai', 'man', 'mang', 'mao', 'me', 'mei', 'men', 'meng',
    'mi', 'mian', 'miao', 'mie', 'min', 'ming', 'miu', 'mo', 'mou', 'mu',

    'na', 'nai', 'nan', 'nang', 'nao', 'ne', 'nei', 'nen', 'neng', #'ng',
    'ni', 'nian', 'niang', 'niao', 'nie', 'nin', 'ning', 'niu', 'nong',
    'nou', 'nu', 'nv', 'nuan', 'nve', 'nuo', 'nun',

    'o', 'ou',

    'pa', 'pai', 'pan', 'pang', 'pao', 'pei', 'pen', 'peng', 'pi', 'pian',
    'piao', 'pie', 'pin', 'ping', 'po', 'pou', 'pu',

    'qi', 'qia', 'qian', 'qiang', 'qiao', 'qie', 'qin', 'qing', 'qiong',
    'qiu', 'qu', 'quan', 'que', 'qun',

    'ran', 'rang', 'rao', 're', 'ren', 'reng', 'ri', 'rong', 'rou', 'ru',
    'ruan', 'rui', 'run', 'ruo',

    'sa', 'sai', 'san', 'sang', 'sao', 'se', 'sen', 'seng', 'sha', 'shai',
    'shan', 'shang', 'shao', 'she', 'shei', 'shen', 'sheng', 'shi', 'shou',
    'shu', 'shua', 'shuai', 'shuan', 'shuang', 'shui', 'shun', 'shuo', 'si',
    'song', 'sou', 'su', 'suan', 'sui', 'sun', 'suo',

    'ta', 'tai', 'tan', 'tang', 'tao', 'te', 'teng', 'ti', 'tian', 'tiao',
    'tie', 'ting', 'tong', 'tou', 'tu', 'tuan', 'tui', 'tun', 'tuo',

    'wa', 'wai', 'wan', 'wang', 'wei', 'wen', 'weng', 'wo', 'wu',

    'xi', 'xia', 'xian', 'xiang', 'xiao', 'xie', 'xin', 'xing', 'xiong',
    'xiu', 'xu', 'xuan', 'xue', 'xun',

    'ya', 'yan', 'yang', 'yao', 'ye', 'yi', 'yin', 'ying', 'yo', 'yong',
    'you', 'yu', 'yuan', 'yue', 'yun',

    'za', 'zai', 'zan', 'zang', 'zao', 'ze', 'zei', 'zen', 'zeng', 'zha',
    'zhai', 'zhan', 'zhang', 'zhao', 'zhe', 'zhei', 'zhen', 'zheng', 'zhi',
    'zhong', 'zhou', 'zhu', 'zhua', 'zhuai', 'zhuan', 'zhuang', 'zhui',
    'zhun', 'zhuo', 'zi', 'zong', 'zou', 'zu', 'zuan', 'zui', 'zun', 'zuo',
);

my %pinyin_lookup;

foreach my $base (@pinyin_bases) {
  my @tones = expandPinyin($base);
  $pinyin_lookup{$base} = [@tones];
}

my $pinyinSearchQuery = "www.chinese-tools.com/tools/sinograms.html?p=";

my $command = "wget -O zong.html ".$pinyinSearchQuery."zong";
#system($command);

local $/;
open(INFILE, './zong.html') || die $!;
my $html = <INFILE>;
close INFILE;

my $dom = Mojo::DOM->new($html);
my @candidates = $dom->find('a')->map(attr => 'title')->each;
my @characters = ();

foreach my $title (@candidates) {
  if (defined $title) {
    $title =~ /(?<=« ).*(?= »)/;
    if (defined $&) {
      push(@characters, $&);
    }
  }
}

my $tone_set = ToneSet->new('zong', $pinyin_lookup{'zong'});

my $translateQuery = buildGoogleTranslateRequest(@characters);
my $command = "wget -O gtzong.html ".$translateQuery;
system($command);




#open OUTFILE, ">", "./pinyin.txt" || die $!;

#for my $result (@results) {
#  for (my $i=0; $i < (length $result); $i++) {
#    print OUTFILE $result[$i].", ";
#  }
#  print OUTFILE "\n";
#}

#close OUTFILE;
