//
//  BridgingHeader.h
//  Flashcards
//
//  Created by Alex Beverage on 20/05/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

#ifndef Flashcards_BridgingHeader_h
#define Flashcards_BridgingHeader_h

#import <Foundation/Foundation.h>

//  hpple:
#import "hpple/TFHpple.h"
#import "hpple/TFHppleElement.h"
#import "hpple/XPathQuery.h"

#endif
