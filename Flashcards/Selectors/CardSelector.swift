//
//  CardSelector.swift
//  Flashcards
//
//  Created by Alex Beverage on 03/06/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

import Foundation

public protocol CardSelector: class {
    var cards: [Card] { get }
    init(deck: Deck)
    func nextCard() -> Card
}

func shuffle<T>(inout items: [T]) -> [T] {
    
    for i in 0..<items.count-1 {
        //  0|1........49-49
        //    1|2......49-48
        //     2|3.....49-47
        //      3|4....49-46
        //          48.49-1
        let target = randomInt(items.count - i) + i
        swap(&items, i, target)
    }
    
    return items
}

func swap<T>(inout items: [T], l: Int, r: Int) {
    let temp = items[l]
    items[l] = items[r]
    items[r] = temp
}

func randomInt(upperBound: Int) -> Int {
    return Int(arc4random_uniform(UInt32(upperBound)))
}