//
//  SimpleSelector.swift
//  Flashcards
//
//  Created by Alex Beverage on 03/06/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

import Foundation

class SimpleSelector: CardSelector {
    
    let deck: Deck
    
    var cards: [Card]
    var cursor = 0
    
    required init(deck: Deck) {
        self.deck = deck
        
        var temp = deck.cards.array as! [Card]
        cards = [Card](temp[0..<deck.cards.array.count])
        shuffle(&cards)
    }
    
    func nextCard() -> Card {
        if cursor == cards.count {
            shuffle(&cards)
            cursor = 0
        }
        
        return cards[cursor++]
    }
}