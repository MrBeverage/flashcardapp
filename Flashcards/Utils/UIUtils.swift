//
//  UIUtils.swift
//  Flashcards
//
//  Created by Alex Beverage on 27/04/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

import Foundation
import UIKit

public class UIUtils {
    
    public class func loadNib(parentView: UIView, nibName: String) -> UIView {
        
        let bundle = NSBundle(forClass: parentView.dynamicType)
        let nib = UINib(nibName: nibName, bundle: bundle)
        
        // Assumes UIView is top level and only object in CustomView.xib file
        let view = nib.instantiateWithOwner(parentView, options: nil)[0] as! UIView
        
        // use bounds not frame or it'll be offset
        view.frame = parentView.bounds
        
        // Make the view stretch with containing view
        view.autoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        parentView.addSubview(view)
        
        return view
    }
}

extension UIView {
    
    func scrollToY(y: CGFloat) {
        UIView.beginAnimations("registerScroll", context: nil)
        UIView.setAnimationCurve(UIViewAnimationCurve.EaseInOut)
        UIView.setAnimationDuration(0.4)
        transform = CGAffineTransformMakeTranslation(CGFloat(0), y)
        UIView.commitAnimations()
    }
    
    func scrollToView(view: UIView) {
        var y = view.frame.origin.y - 15;
        y -= y / 1.7
        scrollToY(-y)
    }
    
    public func loadNib(parentView: UIView, nibName: String) -> UIView {
        
        let bundle = NSBundle(forClass: parentView.dynamicType)
        let nib = UINib(nibName: nibName, bundle: bundle)
        
        // Assumes UIView is top level and only object in CustomView.xib file
        let objects = nib.instantiateWithOwner(parentView, options: nil)
        let view = nib.instantiateWithOwner(parentView, options: nil)[0] as! UIView
        
        // use bounds not frame or it'll be offset
        view.frame = parentView.bounds
        
        // Make the view stretch with containing view
        view.autoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        parentView.addSubview(view)
        
        return view
    }

    public func liveDebugLog(message: String) {
        #if !(TARGET_OS_IPHONE)
            let logPath = "/tmp/XcodeLiveRendering.log"
            if !NSFileManager.defaultManager().fileExistsAtPath(logPath) {
                NSFileManager.defaultManager().createFileAtPath(logPath, contents: NSData(), attributes: nil)
            }
            
            if let fileHandle = NSFileHandle(forWritingAtPath: logPath) {
                fileHandle.seekToEndOfFile()
                
                let date = NSDate()
                let bundle = NSBundle(forClass: self.dynamicType)
                let application: AnyObject = bundle.objectForInfoDictionaryKey("CFBundleName")!
                let data = "\(date) \(application) \(message)\n".dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
                fileHandle.writeData(data!)
            }
        #endif
    }
}