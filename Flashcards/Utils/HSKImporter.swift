//
//  HSKImporter.swift
//  Flashcards
//
//  Util class for validating and importing data from the HSK lists.
//
//  Created by Alex Beverage on 16/05/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

import Foundation
import CoreData
import SwiftyJSON
import JSQCoreDataKit

public class HSKImporter {
    
    public class func loadHSKDecks(deckNames: [String]) -> [HSKDeck] {
        
        var decks: [HSKDeck] = []
        
        for name in deckNames {
            let deckJsonPath = NSBundle.mainBundle().pathForResource(name, ofType: "json")
            let deckResult = HSKDeck.loadDeckFromJsonFile(deckJsonPath!)
            
            if (deckResult.deck != nil) {
                decks.append(deckResult.deck!)
            } else {
                NSLog("\(deckResult.error)")
            }
        }
        
        return decks
    }
    
    public class func convertHSKDecksToCoreData(context: NSManagedObjectContext, hskDecks: [HSKDeck]) -> [Deck] {
        
        var decks: [Deck] = []
        
        for hskDeck in hskDecks {
            var deck  = Deck(context: context, name: hskDeck.title, shortName: hskDeck.shortTitle, deckDescription: hskDeck.description, cards: NSMutableOrderedSet(), locked: !hskDeck.isCustom)
            
            var cards = convertHSKCardsToCoreData(context, hskCards: hskDeck.cards, deck: deck)
            
            deck.cards = NSMutableOrderedSet(array: cards)
        }
        
        return decks
    }
    
    public class func convertHSKCardsToCoreData(context: NSManagedObjectContext, hskCards: [HSKCard], deck: Deck) -> [Card] {
        
        var cards: [Card] = []
        
        for hskCard in hskCards {
            var newCharacters: [Hanzi] = []
            
            //  In retrospect, my way of structuring the JSON wasn't the best:
            for i in 0..<count(hskCard.traditional) {
                
                //  Still making the first transliteration the preferred one.
                let pinyin = split(hskCard.pinyin[0]) { $0 == " " }
                let character = hskCard.traditional[i]
                
                if let hanzi = Hanzi.fetch(context, traditional: character) {
                    //NSLog("Found existing character \(character)")
                    newCharacters.append(hanzi)
                } else {
                    //NSLog("Adding new character \(character)")
                    let hanzi = Hanzi(context: context, traditional: character, simplified: hskCard.simplified[i], transliteration: pinyin[i], audio: nil)
                    newCharacters.append(hanzi)
                }
            }
            
            let content = CardContent(context: context, traditional: hskCard.traditional, simplified: hskCard.simplified, pinyin: hskCard.pinyin[0], definitions: StringUtils.flatten(hskCard.definitions, separator: "\n"), audio: nil)
            
            let card = Card(context: context, questionType: .Text, answerType: .Text, dateAdded: NSDate(), timesViewed: 0, content: content, scoreData: CardScoreData(context: context), deck: deck)
            
            NSLog("Adding card \(content.traditional)")
            
            cards.append(card)
        }
        
        return cards
    }
    
    //  Examines an HSK deck and checks for invalid cards based on a few
    //  common mistakes.
    public class func listInvalidCards(deck: HSKDeck) -> [HSKCard] {
        
        //  Common pattern 1: spaces in the pinyin string.
        let badCards = deck.cards.filter { (card: HSKCard) -> Bool in
            let badPinyin = card.pinyin.filter { (pinyin: String) -> Bool in
                pinyin.rangeOfString("[0-9]", options: .RegularExpressionSearch)?.isEmpty == false
            }
            return badPinyin.count > 0
        }
        
        return badCards
    }
}

public class HSKCard {
    let simplified : String
    let traditional : String
    let pinyin : [String]
    let definitions : [String]
    
    init (simplified: String, traditional: String, pinyin: [String], definitions: [String]) {
        self.simplified = simplified
        self.traditional = traditional
        self.pinyin = pinyin
        self.definitions = definitions
    }
    
    init (dictionary : NSDictionary) {
        simplified  = dictionary["simplified"]  as! String
        traditional = dictionary["traditional"] as! String
        pinyin      = dictionary["pinyin"]      as! [String]
        definitions = dictionary["definitions"] as! [String]
    }
}

public class HSKDeck {
    let isCustom : Bool
    let title : String
    let shortTitle : String
    let description : String
    var cards : [HSKCard]
    
    init (dictionary: NSDictionary) {
        
        title = dictionary["title"] as! String
        shortTitle = dictionary["shortTitle"] as! String
        isCustom = (dictionary["isCustom"] as! NSString).boolValue
        description = dictionary["description"] as! String
        
        let deckJson = dictionary["cards"] as! [NSDictionary]
        var cardsTemp : [HSKCard] = []
        for cardJson in deckJson {
            cardsTemp.append(HSKCard(dictionary: cardJson))
        }
        cards = cardsTemp
    }
    
    init (title: String, shortTitle: String, description: String) {
        self.title = title
        self.shortTitle = shortTitle
        self.description = description
        self.isCustom = true
        self.cards = []
    }
    
    class func loadDeckFromJsonFile(path : String) -> (deck: HSKDeck?, error: NSError?)
    {
        var error : NSError? = nil
        
        if let
            data = NSData(contentsOfFile: path, options:nil, error: &error),
            json = NSJSONSerialization.JSONObjectWithData(data, options: nil, error: &error) as? NSDictionary
        {
            return (HSKDeck(dictionary: json), nil)
        } else {
            return (nil, error)
        }
    }
}

/*
public extension String {
    subscript (i: Int) -> String {
        return String(self[advance(self.startIndex, i)])
    }
}*/