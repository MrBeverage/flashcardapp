//
//  AnswerCardViewClass.swift
//  Flashcards
//
//  Created by Alex Beverage on 26/04/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import AVFoundation

@IBDesignable
class AnswerCardViewClass : UIView {
    
    private var cardView : UIView!
    
    @IBOutlet weak var cardCharacters: UILabel!
    @IBOutlet weak var cardTransliteration: TransliterationView!
    @IBOutlet weak var cardDefinition: UILabel!
    
    weak var audioHandler: CardAudioHandler? = nil {
        didSet {
            cardTransliteration?.audioHandler = audioHandler
        }
    }
    
    func showFront() {
        cardTransliteration.hidden = true
        cardDefinition.hidden = true
    }
    
    func showBack() {
        cardTransliteration.hidden = false
        cardDefinition.hidden = false
    }

    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        cardView = UIUtils.loadNib(self, nibName: "AnswerCardView")
    }
    
    required init(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        
        cardView = UIUtils.loadNib(self, nibName: "AnswerCardView")
    }
}
