//
//  TransliterationView.swift
//  Flashcards
//
//  Created by Alex Beverage on 21/05/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

import UIKit

@IBDesignable class TransliterationView: UIView {

    //  Just for the interface builder:
    private var transliteration: String = ""
    
    private var view: UIView?
    
    var audioHandler: CardAudioHandler?
    
    @IBOutlet weak var transliterationText: UIInsetLabel!
    @IBOutlet weak var audioButton: UIButton!
    
    @IBOutlet var tapGestureRecognizer: UITapGestureRecognizer!
    
    @IBAction func audioButtonPressed(sender: AnyObject) {
        if audioButton.enabled {
            playAudio()
        }
    }
    
    @IBAction func tapped(sender: UITapGestureRecognizer) {
        if audioButton.enabled {
            playAudio()
        }
    }
    
    func playAudio() {
        audioButton.alpha = 0.5
        audioButton.enabled = false
        audioHandler?.playCardAudio()
    }
    
    func audioPlaybackCompleted(error: NSError?) {
        audioButton.alpha = 1
        audioButton.enabled = true
    }
    
    func updateTransliterationText(text: String) {
        transliteration = text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        transliterationText.text = transliteration
        
        audioButton.hidden = text.isEmpty
        audioButton.enabled = !audioButton.hidden
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        view = loadNib(self, nibName: "TransliterationView")
        updateTransliterationText(transliteration)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        view = loadNib(self, nibName: "TransliterationView")
        updateTransliterationText(transliteration)
    }
}
