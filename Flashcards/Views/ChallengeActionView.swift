//
//  ChallengeActionView.swift
//  Flashcards
//
//  Created by Alex Beverage on 03/06/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class ChallengeActionView: UIView {
    
    private var view: UIView!
    
    var actionHandler: CardActionHandler?
    
    @IBAction func flipButtonPressed(sender: UIButton) {
        actionHandler?.cardAction(.Flip)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        view = loadNib(self, nibName: "ChallengeActionView")
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        view = loadNib(self, nibName: "ChallengeActionView")
    }
}