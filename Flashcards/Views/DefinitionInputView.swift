//
//  DefinitionInputView.swift
//  Flashcards
//
//  Created by Alex Beverage on 8/17/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class DefinitionInputView: UIView, UITextViewDelegate {

    private var view: UIView!
    
    @IBOutlet weak var textInput: UITextView!
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var textButton: UIButton!
    @IBOutlet weak var imageButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var completedButton: UIButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        view = loadNib(self, nibName: "DefinitionInputView")
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        view = loadNib(self, nibName: "DefinitionInputView")
    }
    
    func isTextMode() -> Bool {
        return textInput.hidden == false
    }
    
    func isImageMode() -> Bool {
        return imageView.hidden == false
    }
    
    func reset() {
        textInput.text = ""
        imageView.image = nil
        
        backButtonClicked(self)
    }
    
    func getDefinitionText() -> String {
        return textInput.text
    }
    
    func getDefinitionImage() -> UIImage? {
        return nil
    }
    
    @IBAction func textButtonClicked(sender: AnyObject) {
        textButton.hidden = true
        imageButton.hidden = true
        
        backButton.hidden = false
        textInput.hidden = false
        completedButton.hidden = false
    }
    
    @IBAction func imageButtonClicked(sender: AnyObject) {
        textButton.hidden = true
        imageButton.hidden = true
        
        backButton.hidden = false
        imageView.hidden = false
        
        //  Also segue to camera mode.
    }
    
    @IBAction func backButtonClicked(sender: AnyObject) {
        if textInput.isFirstResponder() {
            textInput.resignFirstResponder()
        }
        
        backButton.hidden = true
        imageView.hidden = true
        textInput.hidden = true
        
        completedButton.hidden = true
        textButton.hidden = false
        imageButton.hidden = false
    }
    
    @IBAction func completedButtonClicked(sender: AnyObject) {
        textInput.resignFirstResponder()
    }
}
