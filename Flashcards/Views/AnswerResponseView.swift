//
//  UIAnswerResponseView.swift
//  Flashcards
//
//  Created by Alex Beverage on 26/04/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class AnswerResponseViewClass : UIView {
    
    var buttonRowView: UIView!
   
    weak var resultHandler: CardResultHandler?
    
    @IBAction func failPressed(sender: AnyObject) {
        resultHandler!.cardResult(CardResult.Fail)
    }
    
    @IBAction func hardPressed(sender: AnyObject) {
        resultHandler!.cardResult(CardResult.Hard)
    }
    
    @IBAction func goodPressed(sender: AnyObject) {
        resultHandler!.cardResult(CardResult.Good)
    }
    
    @IBAction func easyPressed(sender: AnyObject) {
        resultHandler!.cardResult(CardResult.Easy)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        buttonRowView = loadNib(self, nibName: "AnswerResponseView")
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        buttonRowView = loadNib(self, nibName: "AnswerResponseView")
    }
}