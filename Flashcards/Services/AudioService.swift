//
//  AudioService.swift
//  Flashcards
//
//  Created by Alex Beverage on 25/05/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

import Foundation
import AVFoundation
import CoreData
import JSQCoreDataKit

public class AudioService {
    
    private var player: AVAudioPlayer?
    private lazy var translateService = TranslateService()

    func playAudio(audio: NSData, completionHandler: (NSError?) -> ()) {
        
        var error: NSError? = nil
        
        player = AVAudioPlayer(data: audio, fileTypeHint: AVFileTypeMPEGLayer3, error: &error)
        
        if error == nil {
            player!.play()
        } else {
            NSLog("Unable to create AVAudioPlayer: \(error)")
        }
        
        completionHandler(error)
    }
    
    func playAudio(audio: [NSData], completionHandler: (NSError?) -> ()) {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            
            var players = [AVAudioPlayer]()
            var error: NSError? = nil
            
            //  I'm an idiot for trying to optimize for space so early by storing each individual
            //  character's audio instead of each card's audio.  This is ugly, but sort of works for
            //  now.  I should probably just stop caring about people's free space.
            for data in audio {
                var player = AVAudioPlayer(data: data, fileTypeHint: AVFileTypeMPEGLayer3, error: &error)
                player!.play()
                players.append(player)
                
                usleep(400000)
            }
            
            while players[players.count - 1].playing {
                usleep(10000)
            }
            
            completionHandler(error)
        }
    }
    
    func getCardAudio(context: NSManagedObjectContext, cardContent: CardContent, completionHandler: (NSData, NSError?) -> () ) {
        
        
        if (cardContent.audio != nil) {
            completionHandler(cardContent.audio!, nil)
        } else {

            translateService.getAudio(cardContent.traditional) { text, audio, error in
                if error == nil {
                    cardContent.audio = audio
                } else {
                    //  Do something about the error so the polling below doesn't explode:
                }
            }
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                
                while (cardContent.audio == nil) {
                    usleep(100000)
                }
                
                dispatch_async(dispatch_get_main_queue()) {
                    completionHandler(cardContent.audio!, nil)
                }
            }
        }
    }
    
    func downloadAndSaveMissingAudioForCardCharacters(context: NSManagedObjectContext, text: String) {
        
        for hanzi in (Hanzi.fetchUniqueHanziForCard(context, text: text).filter { $0.audio == nil }) {
            translateService.getAudio(hanzi.traditional) { (text, data, error) in
                if data != nil {
                    let h = Hanzi.fetch(context, traditional: text)
                    h!.audio = data
                    
                    saveContext(context) { (result) in
                        if result.success {
                            NSLog("Saved audio for \(text)")
                        } else {
                            NSLog("Unable to save audio for \(text): \(result.error)")
                        }
                    }
                }
            }
        }
    }
}