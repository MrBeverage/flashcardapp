//
//  AudioService.swift
//  Flashcards
//
//  Created by Alex Beverage on 04/05/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import hpple

//  TODO: this is pulling from several different webservices.  Those should probably be
//  split out into separate classes.
public class TranslateService {
    
    //  For blocking on token updates:
    private static var RefreshingToken: Bool = false
    private static var TokenRefreshLock: NSCondition = NSCondition()
    
    //  For scraping from chinese-tools:
    private static let DictionaryEntries = "//*[@class=\"cwCharBlock\"]"
    private static let SimplifiedXpath = "//div[text()=\"Simplified\"]/following-sibling::*/a/text()"
    
    //  Might want these later:
    //private static let TransliterationsXpath           = "//*[@class=\"cwCharBlockTopP\"]/text()"
    //private static let TransliterationDefinitionsXpath = "//*[@class=\"cwCharBlockGeneral\"]"
    
    //  Returns either the current authentication token, or tries to get a new one.
    //  A lock on the token is taken during the authentication request's round trip.  This
    //  is to keep us from spamming Bing, and to allow concurrent requests to immediately
    //  execute once the first request has acquired a token.
    private func authorizeForBing(handler: (NSError?) -> ()) {
        
        //  Ugh, seriously Apple?
        let startTime = NSDate().timeIntervalSince1970
        
        if Router.TOKEN == nil || Router.TOKEN_EXPIRATION < startTime {
            NSLog("BT auth token has expired.")
            
            TranslateService.TokenRefreshLock.lock()
            
            if (TranslateService.RefreshingToken == false) {
                TranslateService.RefreshingToken = true
            } else {
                //  Chance of race condition here is infitesmal, but does exist.  Consequence
                //  would be two concurrent auth requests, which wouldn't break anything.
                TranslateService.TokenRefreshLock.wait()
            }
            
            //  Do I need this check anymore?
            if Router.TOKEN == nil || Router.TOKEN_EXPIRATION < startTime {
                NSLog("Refreshing BT auth token.")
                
                let requestStartTime = startTime
                
                Alamofire.request(Router.GetAuthToken())
                    .responseJSON { (request, response, json, error) in
                    
                    if error != nil {
                        NSLog("Error getting new BT auth token: \(error)")
                        
                        Router.TOKEN = nil
                        Router.TOKEN_EXPIRATION = nil
                        Router.TOKEN_ERROR = error
                        
                        TranslateService.TokenRefreshLock.broadcast()
                    } else {
                        let response = JSON(json!)
                        let token = response["access_token"].string
                        let expires_in = response["expires_in"].string
                        
                        Router.TOKEN = token!
                        Router.TOKEN_EXPIRATION = requestStartTime + Double(expires_in!.toInt()!)
                        Router.TOKEN_ERROR = nil
                        
                        TranslateService.RefreshingToken = false
                        
                        TranslateService.TokenRefreshLock.broadcast()
                    }
                }
                
                while Router.TOKEN == nil || Router.TOKEN_EXPIRATION < startTime {
                    TranslateService.TokenRefreshLock.wait()
                }
            }
            
            TranslateService.TokenRefreshLock.unlock()
        }
        
        NSLog("Using BT auth token \(self.getShortToken())")
        handler(Router.TOKEN_ERROR)
    }
    
    //  For logging purposes.
    private func getShortToken() -> String {
        let startIndex = advance(Router.TOKEN!.startIndex, count(Router.TOKEN!) - 8)
        return Router.TOKEN![startIndex..<Router.TOKEN!.endIndex]
    }
    
    public func getAudio(text: String, audioHandler: (text: String, audio: NSData?, error: NSError?) -> ()) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            self.authorizeForBing() { (authError) in
                
                if authError == nil {
                    NSLog("Downloading audio for \(text)")
                    Alamofire.request(Router.GetAudio(text: text))
                        .response { (_, _, data, error) in
                            audioHandler(text: text, audio: data as? NSData, error: error)
                    }
                } else {
                    audioHandler(text: text, audio: nil, error: authError)
                }
            }
        }
    }
    
    public func lookupTraditional(traditionalCharacter: String, responseHandler: (traditional: String, simplified: String?, transliteration: String?, error: NSError?) -> ()) {
        
        Alamofire.request(Router.GetPinyin(character: traditionalCharacter))
            .responseString() { _, _, htmlString, error in
        
                var simplified: String? = nil
                var transliteration: String? = nil
                
                if error == nil {
                    let htmlNSString: NSString = htmlString! as NSString
                    let data = htmlNSString.dataUsingEncoding(NSUTF8StringEncoding)
                    
                    let parser  = TFHpple(data: data, isXML: false)
                    let transliterationMatches = parser.searchWithXPathQuery(TranslateService.DictionaryEntries) as! [TFHppleElement]
                    
                    //  Select the entry with the most definitions for now.  It most likely has the
                    //  right pronounciation.  This is a problem for characters like 行, and less so
                    //  石, where the same character can have completely different (not just by tone)
                    //  pronounciations.
                    
                    //  Another thing to watch out for: the website that's being scraped uses different
                    //  formats for different characters in a pattner I haven't quite figured out.
                    //  One looks like it is meant to be an upgrade to the other, however the data on
                    //  the upgraded pages is also included in the old format, but hidden.  The 'older'
                    //  pages do not have the same data in the new format.  Using the old style for now.
                    if transliterationMatches.count > 0 {
                        
                        var candidate: TFHppleElement? = transliterationMatches[0]
                        var candidateDefinitions = -1
                        
                        for match in transliterationMatches {
                            //  This _should_ always work...
                            let current = (match.children as! [TFHppleElement])
                                .filter { $0.attributes["class"] as! String == "cwCharBlockGeneral" }
                            
                            if current.count > 0 && (candidate == nil || candidateDefinitions < current[0].children.count) {
                                candidate = match
                                candidateDefinitions = current[0].children.count
                            }
                        }
                        
                        //  The transliteration is div[0]/span[1] below the candidate node.
                        //  This is just plain awful.  There must be some better Xpath-FU that can be done here.
                        //  Figure that out later.
                        let transliterationNode =
                            (candidate?.childrenWithTagName("div")[0]  as! TFHppleElement)
                                       .childrenWithTagName("span")[1] as! TFHppleElement
                        
                        transliteration = transliterationNode.text()
                    }
                    
                    let simplifiedMatch = parser.searchWithXPathQuery(TranslateService.SimplifiedXpath)
                    
                    if simplifiedMatch.count > 0 {
                        if let element = simplifiedMatch[0] as? TFHppleElement {
                            NSLog("Found simplified " + element.raw + " for " + traditionalCharacter)
                            simplified = element.raw
                        }
                    } else {
                        simplified = traditionalCharacter
                    }
                }
                
                responseHandler(traditional: traditionalCharacter, simplified: simplified, transliteration: transliteration, error: error)
        }
    }
}


enum Router: URLRequestConvertible {

    private static let AUTH_URL   = "https://datamarket.accesscontrol.windows.net/v2/OAuth2-13/"
    private static let SCOPE      = "http://api.microsofttranslator.com"
    private static let GRANT_TYPE = "client_credentials"
    private static let VERSION    = "V2"
    
    private static let CLIENT_SECRET = "EiZW7gKgt42ZQzOMxsRTcs/V2+7ZXXiq+VJTpdg0x/I="
    private static let CLIENT_ID = "beverage-flashcard-app"
    
    private static let AUTH_REQUEST_PARAMS : [String: String] = [
        "client_id"     : CLIENT_ID,
        "client_secret" : CLIENT_SECRET,
        "scope"         : SCOPE,
        "grant_type"    : GRANT_TYPE
    ]
    
    private static var PINYIN_URL = "http://www.chinese-tools.com/tools/sinograms.html"
    
    //  Exposing the token as a global anyone can update.  Ugh.  It works though, 
    //  and keeps everything Alamofire-pattern-compliant as far as I can understand yet.
    static var TOKEN : String?
    static var TOKEN_EXPIRATION : Double?
    static var TOKEN_ERROR : NSError?
    
    case GetAuthToken()
    case GetAudio(text : String)
    case GetPinyin(character: String)
    
    var method : Alamofire.Method {
        switch self {
        case .GetAudio:
            return .GET
        case .GetAuthToken:
            return .POST
        case .GetPinyin:
            return .GET
        }
    }
    
    var path : String {
        switch self {
        case .GetAudio:
            return "\(Router.SCOPE)/\(Router.VERSION)/Http.svc/Speak"
        case .GetAuthToken:
            return Router.AUTH_URL
        case .GetPinyin:
            return Router.PINYIN_URL
        }
    }
    
    var parameters : [String : String] {
        switch self {
        case .GetAudio(let text):
            return [ "text": text, "language": "zh-CHT", "format": "audio/mp3" ]
        case .GetAuthToken():
            return Router.AUTH_REQUEST_PARAMS
        case .GetPinyin(let character):
            return [ "q": character ]
        }
    }
    
    var URLRequest : NSURLRequest {
        let url = NSURL(string: path)!
        let urlRequest = NSMutableURLRequest(URL: url)
        urlRequest.HTTPMethod = method.rawValue

        switch self {
        case .GetAudio:
            urlRequest.setValue("Bearer \(Router.TOKEN!)", forHTTPHeaderField: "Authorization")
            urlRequest.setValue("audio/mp3", forHTTPHeaderField: "Content-Type")
        default:
            var dummy = 1
        }
    
        let encoding = Alamofire.ParameterEncoding.URL
        return encoding.encode(urlRequest, parameters: parameters).0
    }
}
