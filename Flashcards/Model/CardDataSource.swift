//
//  CardDataSource.swift
//  Flashcards
//
//  Created by Alex Beverage on 29/05/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

import Foundation
import CoreData
import UIKit
import JSQCoreDataKit

typealias CardCellConfigurationBlock = (cell: CardListCell, indexPath: NSIndexPath, card: Card?) -> ()

class CardDataSource: NSObject, RequiresCoreData {
 
    static let TableCellName = "CardCell"
    
    private var context: NSManagedObjectContext? = nil
    var stack: CoreDataStack? = nil {
        didSet {
            context = stack?.managedObjectContext
        }
    }
    
    private var cardsByType = [CardType: [Card]]()
    var deck: Deck? = nil {
        didSet {
            cardsByType = sortCardsByType(deck)
        }
    }
    
    var cardCellConfigurationBlock: CardCellConfigurationBlock?
    
    private func sortCardsByType(deck: Deck?) -> [CardType: [Card]] {
        var table = [CardType: [Card]]()
        
        for cardType in CardType.allTypes {
            let cardsForType = Card.fetchCardsOfTypeForDeck(deck!, cardType: cardType, sorted: true)
            if cardsForType.count > 0 {
                table[cardType] = cardsForType
            }
        }
        
        return table
    }
}

extension CardDataSource: UITableViewDataSource {
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(CardDataSource.TableCellName) as! CardListCell
        
        if let configureBlock = cardCellConfigurationBlock,
            cardType = CardType(rawValue: Int16(indexPath.section)) {
                
            configureBlock(cell: cell, indexPath: indexPath, card: cardsByType[cardType]?[indexPath.row])
        }
        
        return cell
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return cardsByType.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let cardType = CardType(rawValue: Int16(section)) {
            return cardsByType[cardType]?.count ?? 0
        } else {
            return 0
        }
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if let cardType = CardType(rawValue: Int16(section)) {
            return cardType.getCategoryDisplayName()
        } else {
            return "Unknown category"
        }
    }
    
    // MARK: - Table view data source
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return NO if you do not want the specified item to be editable.
    return true
    }
    */
    
    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    if editingStyle == .Delete {
    // Delete the row from the data source
    tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
    } else if editingStyle == .Insert {
    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
    }
    */
    
    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
    
    }
    */
    
    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return NO if you do not want the item to be re-orderable.
    return true
    }
    */
}
