//
//  Card.swift
//  
//
//  Created by Alex Beverage on 27/06/15.
//
//

import Foundation
import CoreData

public final class Card: NSManagedObject {

    @NSManaged var dateAdded: NSDate
    @NSManaged var timesViewed: Int32
    @NSManaged var content: CardContent
    @NSManaged var scoreData: CardScoreData
    @NSManaged var deck: Deck
    
    @NSManaged private var questionTypeInternal: Int16
    var questionType: CardType {
        get { return CardType(rawValue: questionTypeInternal) ?? .Text }
        set { self.questionTypeInternal = newValue.rawValue }
    }

    @NSManaged private var answerTypeInternal: Int16
    var answerType: CardType {
        get { return CardType(rawValue: answerTypeInternal) ?? .Text }
        set { self.answerTypeInternal = newValue.rawValue }
    }
    public override init(entity: NSEntityDescription, insertIntoManagedObjectContext: NSManagedObjectContext?) {
        super.init(entity: entity, insertIntoManagedObjectContext: insertIntoManagedObjectContext)
    }
    
    public init(context: NSManagedObjectContext,
        questionType: CardType,
        answerType: CardType,
        dateAdded: NSDate,
        timesViewed: Int32,
        content: CardContent,
        scoreData: CardScoreData,
        deck: Deck) {
            
            let entity = NSEntityDescription.entityForName(EntityNames.Card, inManagedObjectContext: context)!
            super.init(entity: entity, insertIntoManagedObjectContext: context)
            
            self.questionType = questionType
            self.answerType = answerType
            self.dateAdded = dateAdded
            self.timesViewed = timesViewed
            self.content = content
            self.scoreData = scoreData
            self.deck = deck
    }
}
