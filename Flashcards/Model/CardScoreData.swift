//
//  CardScoreData.swift
//  Flashcards
//
//  Created by Alex Beverage on 8/14/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

import Foundation
import CoreData

public final class CardScoreData: NSManagedObject {
    
    public override init(entity: NSEntityDescription, insertIntoManagedObjectContext: NSManagedObjectContext?) {
        super.init(entity: entity, insertIntoManagedObjectContext: insertIntoManagedObjectContext)
    }
    
    public init(context: NSManagedObjectContext) {
            let entity = NSEntityDescription.entityForName(EntityNames.CardScoreData, inManagedObjectContext: context)!
            super.init(entity: entity, insertIntoManagedObjectContext: context)
    }
}