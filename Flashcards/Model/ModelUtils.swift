//
//  ModelUtils.swift
//  Flashcards
//
//  Created by Alex Beverage on 17/05/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

import Foundation
import CoreData
import JSQCoreDataKit

public let ModelName = "Flashcards"

public let ModelBundle = NSBundle(identifier: "org.beverage.Flashcards")!

public struct EntityNames {
    public static let Card = "Card"
    public static let CardContent  = "CardContent"
    public static let CardScoreData = "CardScoreData"
    public static let Deck  = "Deck"
    public static let Hanzi = "Hanzi"
}

extension Hanzi {
    
    public class func fetch(context: NSManagedObjectContext, predicate: NSPredicate?) -> [Hanzi] {
        let entity  = JSQCoreDataKit.entity(name: EntityNames.Hanzi, context: context)
        let request = FetchRequest<Hanzi>(entity: entity)
        request.predicate = predicate
        //  TODO: will need columnar queries later - pulling the audio data is not always needed
        //  and could be quite expensive.
        
        let result = JSQCoreDataKit.fetch(request: request, inContext: context)
        
        if !result.success {
            println("Error = \(result.error)")
        }
        
        return result.objects
    }
    
    public class func fetch(context: NSManagedObjectContext, traditional: String) -> Hanzi? {
        let result = fetch(context, predicate: NSPredicate(format: "traditional == %@", traditional))
        return result.count > 0 ? result[0] : nil
    }
    
    public class func fetchAll(context: NSManagedObjectContext) -> [Hanzi] {
        return fetch(context, predicate: nil)
    }
    
    public class func fetchUniqueHanziForCard(context: NSManagedObjectContext, card: Card) -> [Hanzi] {
        return fetchUniqueHanziForCard(context, text: card.content.traditional)
    }
    
    public class func fetchUniqueHanziForCard(context: NSManagedObjectContext, text: String) -> [Hanzi] {
        
        var table = [Hanzi: Bool]()
        
        for c in text {
            if StringUtils.isChineseCharacter(c) {
                if let hanzi = Hanzi.fetch(context, traditional: String(c)) {
                    table[hanzi] = true
                }
            }
        }
        
        return table.keys.array
        
        //  This predicate doesn't seem to work for SQLite:
        //return fetch(context, predicate: NSPredicate(format: "traditional IN %@", card.traditional))
    }
}

extension Card {
    
    public class func fetchCardsOfTypeForDeck(deck: Deck, cardType: CardType, sorted: Bool) -> [Card] {
        
        if let context = deck.managedObjectContext {
            let entity  = JSQCoreDataKit.entity(name: EntityNames.Card, context: context)
            let request = FetchRequest<Card>(entity: entity)
            
            let predicate = NSPredicate(format:
                "deck.shortName == %@ AND questionTypeInternal == \(cardType.rawValue)", deck.shortName)
            request.predicate = predicate
            
            //  CardType == .Text is temporary - there will probably be different ways we want to sort 
            //  different question/answer types later on:
            if cardType == .Text && sorted == true {
                let sortDescriptor = NSSortDescriptor(key: "content.pinyin", ascending: true, selector: "caseInsensitiveCompare:")
                request.sortDescriptors = [sortDescriptor]
            }
            
            let result = JSQCoreDataKit.fetch(request: request, inContext: context)
            
            if !result.success {
                println("Error = \(result.error)")
            }
            
            return result.objects
        } else {
            return [Card]()
        }
    }
    
    public class func fetch(context: NSManagedObjectContext, predicate: NSPredicate?) -> [Card] {
        let entity  = JSQCoreDataKit.entity(name: EntityNames.Card, context: context)
        let request = FetchRequest<Card>(entity: entity)
        request.predicate = predicate
        
        let result : FetchResult = JSQCoreDataKit.fetch(request: request, inContext: context)
        
        if !result.success {
            println("Error = \(result.error)")
        }
        
        return result.objects
    }
    
    public class func fetch(context: NSManagedObjectContext, cardText: String, deckShortName: String) -> [Card] {
        return fetch(context, predicate: NSPredicate(format: "content.traditional == %@ AND deck.shortName == $@", cardText, deckShortName))
    }
    
    public class func fetchCardHanziArray(context: NSManagedObjectContext, card: Card) -> [Hanzi] {
        return fetchCardHanziArray(context, text: card.content.traditional)
    }
    
    public class func fetchCardHanziArray(context: NSManagedObjectContext, text: String) -> [Hanzi] {
        var hanziTable = [String: Hanzi]()
        var result: [Hanzi] = []
        
        for c in text {
            if StringUtils.isChineseCharacter(c) {
                let character = String(c)
                
                if let h = hanziTable[character] {
                    result.append(h)
                } else if let h = Hanzi.fetch(context, traditional: character) {
                    hanziTable[character] = h
                    result.append(h)
                }
            }
        }
        
        return result
    }
}

extension Deck {
    
    public class func fetch(context: NSManagedObjectContext, predicate: NSPredicate?) -> [Deck] {
        let entity  = JSQCoreDataKit.entity(name: EntityNames.Deck, context: context)
        let request = FetchRequest<Deck>(entity: entity)
        request.predicate = predicate
        
        let result : FetchResult = JSQCoreDataKit.fetch(request: request, inContext: context)
        
        if !result.success {
            println("Error = \(result.error)")
        }
        
        return result.objects
    }
    
    public class func fetch(context: NSManagedObjectContext, shortName: String) -> Deck? {
        //  TODO: make sure uniqueness is actually enforced on shortTitle, or some up with some other ID.
        //  shortName is probably a terrible name for the unique ID anyways.
        let decks = fetch(context, predicate: NSPredicate(format: "shortName == %@", shortName))
        
        return decks.count > 0 ? decks[0] : nil
    }
    
    public class func fetchAllDecks(context: NSManagedObjectContext) -> [Deck] {
        return fetch(context, predicate: nil)
    }
}
