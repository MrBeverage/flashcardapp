//
//  Card.swift
//  Flashcards
//
//  Created by Alex Beverage on 16/05/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

import Foundation
import CoreData

public final class CardContent: NSManagedObject {

    @NSManaged var traditional: String
    @NSManaged var simplified: String
    @NSManaged var pinyin: String
    @NSManaged var definitions: String?
    @NSManaged var audio: NSData?

    public override init(entity: NSEntityDescription, insertIntoManagedObjectContext: NSManagedObjectContext?) {
        super.init(entity: entity, insertIntoManagedObjectContext: insertIntoManagedObjectContext)
    }
    
    public init(context: NSManagedObjectContext,
        traditional: String,
        simplified: String,
        pinyin: String,
        definitions: String?,
        audio: NSData?) {
            
            let entity = NSEntityDescription.entityForName(EntityNames.CardContent, inManagedObjectContext: context)!
            super.init(entity: entity, insertIntoManagedObjectContext: context)
            
            self.traditional = traditional
            self.simplified = simplified
            self.pinyin = pinyin
            self.definitions = definitions
            self.audio = audio
    }
}