//
//  DeckDataSource.swift
//  Flashcards
//
//  Created by Alex Beverage on 06/05/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

import Foundation
import UIKit
import CoreData
import JSQCoreDataKit

typealias DeckCellConfigurationBlock = (cell: DeckTableViewCell, indexPath: NSIndexPath, deck: Deck) -> ()

class DeckDataSource : NSObject, RequiresCoreData {
    
    static let TableCellName = "DeckCell"
    
    //  Test code to seed the deck menu with some pre-defined decks if missing:
    static let StaticDecks = "Static Decks"
    static let CustomDecks = "Custom Decks"
    static let TestDeckNames : [String: [String]] = [
        StaticDecks : [ "hsk1", "hsk2", "hsk3" ],//, "hsk4", "hsk5", "hsk6" ],
        CustomDecks : [ "test", "capitols" ]
    ]
    
    //  CoreData:
    var stack: CoreDataStack? = nil {
        didSet {
            context = stack?.managedObjectContext
        }
    }
    private var context: NSManagedObjectContext?
    
    private var deckLoadLock = NSLock()
    var decks : [String: [Deck]] {
        get {
            var decksByCategory = categorizeDecks(Deck.fetchAllDecks(context!))
            
            if needsInitialLoad(decksByCategory) {
                deckLoadLock.lock()
                if needsInitialLoad(decksByCategory) {
                    decksByCategory = loadTestDecks()
                }
                deckLoadLock.unlock()
            }
            
            return decksByCategory
        }
    }
    
    var deckCellConfigurationBlock : DeckCellConfigurationBlock?

    private func needsInitialLoad(decksByCategory: [String: [Deck]]) -> Bool {
        return decksByCategory[DeckDataSource.StaticDecks]?.count == 0 || decksByCategory[DeckDataSource.CustomDecks]?.count == 0
    }
    
    private func loadTestDecks() -> [String: [Deck]] {
        NSLog("Loading test decks.")
        
        var decks = categorizeDecks(Deck.fetchAllDecks(context!))
        
        if (decks[DeckDataSource.StaticDecks]?.count == 0) {
            decks[DeckDataSource.StaticDecks] = HSKImporter.convertHSKDecksToCoreData(
                context!, hskDecks: HSKImporter.loadHSKDecks(DeckDataSource.TestDeckNames[DeckDataSource.StaticDecks]!))
        }
        
        if (decks[DeckDataSource.CustomDecks]?.count == 0) {
            decks[DeckDataSource.CustomDecks] = HSKImporter.convertHSKDecksToCoreData(
                context!, hskDecks: HSKImporter.loadHSKDecks(DeckDataSource.TestDeckNames[DeckDataSource.CustomDecks]!))
        }
        
        let result = saveContextAndWait(context!)
        
        if (result.success == false) {
            NSLog("Failed to save decks on import: \(result.error)")
        }
        
        return decks
    }

    func createNewDeck(name: String, description: String) {
        let shortName = (split(name) { $0 == " " })[0]

        let deck = Deck(context: context!, name: name, shortName: shortName, deckDescription: description, cards: NSMutableOrderedSet(), locked: false)
        
        let result = saveContextAndWait(context!)
        
        if (result.success == false) {
            NSLog("Failed to save deck \(name): \(result.error)")
        }
    }
    
    //  For now, categories are either static or custom.  This can expand later.
    private func categorizeDecks(decks: [Deck]) -> [String: [Deck]] {
        var decksByCategory = [String: [Deck]]()
        decksByCategory[DeckDataSource.StaticDecks] = []
        decksByCategory[DeckDataSource.CustomDecks] = []
        
        for deck in decks {
            if (deck.locked) {
                decksByCategory[DeckDataSource.StaticDecks]?.append(deck)
            } else {
                decksByCategory[DeckDataSource.CustomDecks]?.append(deck)
            }
        }
        
        for category in decksByCategory.keys {
            decksByCategory[category] = decksByCategory[category]?.sorted { $0.name < $1.name }
        }
        
        return decksByCategory
    }
}

extension DeckDataSource: UITableViewDataSource {
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(DeckDataSource.TableCellName) as! DeckTableViewCell
        let sectionName = decks.keys.array[indexPath.section]
        
        if let configureBlock = deckCellConfigurationBlock {
            configureBlock(cell: cell, indexPath: indexPath, deck: decks[sectionName]![indexPath.row])
        }
        
        return cell
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return decks.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionName = decks.keys.array[section]
        return decks[sectionName]!.count
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return decks.keys.array[section]
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
}
