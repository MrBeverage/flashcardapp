//
//  CardTypes.swift
//  Flashcards
//
//  Created by Alex Beverage on 8/13/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

import Foundation

public enum CardType: Int16 {
    case Text  = 0
    case Image = 1
    case Audio = 2
    
    static let allTypes = [ Text, Image, Audio ]
    
    private static let categoryDisplayNames = [ Text: "Cards", Image: "Pictures", Audio: "Sounds" ]
    
    func getCategoryDisplayName() -> String {
        return CardType.categoryDisplayNames[self]!
    }
}