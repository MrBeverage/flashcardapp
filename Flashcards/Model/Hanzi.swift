//
//  Character.swift
//  Flashcards
//
//  Created by Alex Beverage on 16/05/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

import Foundation
import CoreData

//  Hanzi, because Character just has to be reserved, goddamnit.
public final class Hanzi: NSManagedObject {

    @NSManaged var audio: NSData?
    @NSManaged var simplified: String
    @NSManaged var traditional: String
    @NSManaged var transliteration: String
    
    public override init(entity: NSEntityDescription, insertIntoManagedObjectContext: NSManagedObjectContext?) {
        super.init(entity: entity, insertIntoManagedObjectContext: insertIntoManagedObjectContext)
    }
    
    public init(context: NSManagedObjectContext,
        traditional: String,
        simplified: String,
        transliteration: String,
        audio: NSData?) {
            let entity = NSEntityDescription.entityForName(EntityNames.Hanzi, inManagedObjectContext: context)!
            super.init(entity: entity, insertIntoManagedObjectContext: context)
            
            self.traditional = traditional
            self.simplified = simplified
            self.transliteration = transliteration
            self.audio = audio
    }
}
