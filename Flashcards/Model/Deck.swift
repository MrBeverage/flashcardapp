//
//  Deck.swift
//  Flashcards
//
//  Created by Alex Beverage on 16/05/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

import Foundation
import CoreData
import JSQCoreDataKit

public final class Deck: NSManagedObject {

    @NSManaged var deckDescription: String
    @NSManaged var name: String
    @NSManaged var shortName: String
    @NSManaged var cards: NSMutableOrderedSet
    @NSManaged var locked: Bool

    //  Seems to be required for entities that can contain empty sets of subentities.  Why?
    public override init(entity: NSEntityDescription, insertIntoManagedObjectContext: NSManagedObjectContext?) {
        super.init(entity: entity, insertIntoManagedObjectContext: insertIntoManagedObjectContext)
    }
    
    public init(context: NSManagedObjectContext,
        name: String,
        shortName: String,
        deckDescription: String,
        cards: NSMutableOrderedSet,
        locked: Bool) {
            
            let entity = NSEntityDescription.entityForName(EntityNames.Deck, inManagedObjectContext: context)!
            super.init(entity: entity, insertIntoManagedObjectContext: context)
            
            self.name = name
            self.shortName = shortName
            self.deckDescription = deckDescription
            self.cards = cards
            self.locked = locked
    }
}
