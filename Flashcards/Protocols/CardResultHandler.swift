//
//  ChallengeResultHandler.swift
//  Flashcards
//
//  Created by Alex Beverage on 28/04/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

import Foundation

enum CardResult {
    case Fail;
    case Hard;
    case Good;
    case Easy;
}

protocol CardResultHandler : class {
    func cardResult(result: CardResult)
}