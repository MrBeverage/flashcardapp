//
//  CardActionHandler.swift
//  Flashcards
//
//  Created by Alex Beverage on 03/06/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

import Foundation

enum CardAction {
    case Flip;
}

protocol CardActionHandler: class {
    func cardAction(cardAction: CardAction)
}