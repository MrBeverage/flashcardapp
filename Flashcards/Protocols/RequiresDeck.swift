//
//  DeckViewController.swift
//  Flashcards
//
//  Created by Alex Beverage on 7/27/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

import Foundation

protocol RequiresDeck: class {
    var deck: Deck? { get set }
}