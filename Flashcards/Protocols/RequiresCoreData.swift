//
//  UsesCoreData.swift
//  Flashcards
//
//  Created by Alex Beverage on 19/05/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

import Foundation
import CoreData
import JSQCoreDataKit

protocol RequiresCoreData: class {
    var stack: CoreDataStack? { get set }
}
