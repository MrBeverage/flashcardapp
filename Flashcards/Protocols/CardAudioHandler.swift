//
//  CardAudioHandler.swift
//  Flashcards
//
//  Created by Alex Beverage on 04/05/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

import Foundation

protocol CardAudioHandler : class {
    func playCardAudio() 
}