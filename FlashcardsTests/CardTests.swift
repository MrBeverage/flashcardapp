//
//  FlashcardsTests.swift
//  FlashcardsTests
//
//  Created by Alex Beverage on 22/04/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

import UIKit
import XCTest

class CardTests : XCTestCase {
    
    let hsk1DictionaryExpectedSize = 150
    
    //  Test data in dictionary form in case some data-driven test tool is built or located later.
    let cards : [String:
        (definition: (simplified: String, traditional: String, pinyin: [String], definitions: String),
         hasSimplification: Bool,
         expectedDefinitions: Int)] =
    [
        "oneCharacterNoSimpliciation" :
            (("在", "在", [ "zài" ], "at; on; in; indicates an action in progress"), false, 4),
        "multipleCharactersNoSimplification" :
            (("先生", "先生", [ "xiān", "sheng" ], "Mr.; Sir"), false, 2),
        "multipleCharactersHasSimplification" :
            (("时候", "時候", [ "shí", "hou" ], "time"), true, 1)
    ]
    
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testFindBadCards() {
        let decks: [HSKDeck] = HSKImporter.loadHSKDecks(["hsk1"])
        
        for deck in decks {
            let badCards = HSKImporter.listInvalidCards(deck)
            
            XCTAssertTrue(badCards.count == 0)
            
            if (badCards.count > 0) {
                NSLog("\(deck.shortTitle): \(badCards.count) cards are invalid:")
                for card in badCards {
                    NSLog("\(card.traditional): \(card.pinyin)")
                }
            }
        }
    }
    
    func testCardOneCharacterNoSimplification() {
        let testCase = cards["oneCharacterNoSimpliciation"]!
        let card : HSKCard = HSKCard(simplified: testCase.definition.simplified, traditional: testCase.definition.traditional, pinyin: testCase.definition.pinyin, definitions: [testCase.definition.definitions])
        
        assertCardContentsExist(card)
    }
    
    func testMultipleCharactersNoSimplification() {
        let testCase = cards["multipleCharactersNoSimplification"]!
        let card : HSKCard = HSKCard(simplified: testCase.definition.simplified, traditional: testCase.definition.traditional, pinyin: testCase.definition.pinyin, definitions: [testCase.definition.definitions])
        
        assertCardContentsExist(card)
    }
    
    func testMultipleCharactersHasSimplification() {
        let testCase = cards["multipleCharactersHasSimplification"]!
        let card : HSKCard = HSKCard(simplified: testCase.definition.simplified, traditional: testCase.definition.traditional, pinyin: testCase.definition.pinyin, definitions: [testCase.definition.definitions])
        
        assertCardContentsExist(card)
    }
    
    func testCreateDeck_OldFromJson() {
        let deckJsonPath = NSBundle.mainBundle().pathForResource("hsk1", ofType: "json")

        let result = HSKDeck.loadDeckFromJsonFile(deckJsonPath!)
        let deck = result.deck
        
        XCTAssertNotNil(deck)
        
        if deck == nil {
            return
        }
        
        XCTAssertEqual(deck!.cards.count, hsk1DictionaryExpectedSize, "HSK1 deck does not contain 100 cards.")
        
        for card in deck!.cards {
            assertCardContentsExist(card)
        }
    }
    
    func assertPinyinIsCorrect(card: HSKCard, expectedRomanizations: [(expectedPhoneme: String, expectedTone: Int)]) {
        XCTAssertTrue(card.pinyin.count == expectedRomanizations.count)
        
        if (card.pinyin.count != expectedRomanizations.count) {
            return
        }
    }
    
    func assertDefinitionsAreCorrect(card: HSKCard, expectedDefinitions: Int) {
        XCTAssertTrue(card.definitions.count == expectedDefinitions)
        
        //  Check that definitions were properly trimmed.
        for definition in card.definitions {
            XCTAssertFalse(definition[definition.startIndex] == " ")
        }
    }
    
    func assertCardContentsExist(card: HSKCard) {
        XCTAssertTrue(count(card.simplified) > 0)
        XCTAssertTrue(count(card.traditional) > 0)
        XCTAssertTrue(count(card.pinyin) > 0)
        XCTAssertTrue(count(card.definitions) > 0)
    }
}
