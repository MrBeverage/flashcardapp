//
//  ServiceTests.swift
//  Flashcards
//
//  Created by Alex Beverage on 20/05/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

import UIKit
import XCTest
import Alamofire

class ServiceTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testLookupTraditional() {
        testLookupTraditionalInternal("道", simplified: "道", transliteration: "dào")
        testLookupTraditionalInternal("後", simplified: "后", transliteration: "hòu")  // <- has simplified
        testLookupTraditionalInternal("石", simplified: "石", transliteration: "shí")  // <- want 2nd transliteration
    }
    
    private func testLookupTraditionalInternal(traditional: String, simplified: String, transliteration: String) {
        let service = TranslateService()
        
        weak var expectation = expectationWithDescription("Service returns \(simplified)/\(transliteration) for \(traditional).")
        
        service.lookupTraditional(traditional) { (_, s, t, error) in
            
            NSLog("Callback started")
            
            XCTAssert(s != nil && s == simplified,      "Got \(s)")
            XCTAssert(t != nil && t == transliteration, "Got \(t)")
            
            if (error != nil) {
                NSLog("\(error)")
            }
            
            expectation?.fulfill()
        }
        
        waitForExpectationsWithTimeout(5.0) { (error) in
            if (error != nil) {
                NSLog("\(error)")
            }
        }
    }
}
