//
//  DataTests.swift
//  Flashcards
//
//  Created by Alex Beverage on 17/05/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

import UIKit
import XCTest
import CoreData
import JSQCoreDataKit

class DataTests: XCTestCase {

    var model: CoreDataModel?
    var stack: CoreDataStack?
    
    override func setUp() {
        super.setUp()
        
        model = CoreDataModel(name: ModelName, bundle: ModelBundle)
        
        // Initialize a private queue, in-memory stack
        stack = CoreDataStack(model: model!, storeType: NSInMemoryStoreType, options: nil, concurrencyType: .PrivateQueueConcurrencyType)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testNoHanzi() {
        let context = stack?.managedObjectContext
        
        let hanzi = Hanzi.fetchAll(context!)
        
        XCTAssertTrue(hanzi.count == 0)
    }
    
    func testFetchUniqueHanziForCard() {
        let context = stack?.managedObjectContext
        
        let deck  = Deck(context: context!, name: "test", shortName: "test", deckDescription: "test", cards: NSMutableOrderedSet(), locked: false)
        let hanzi = [Hanzi(context: context!, traditional: "好", simplified: "好", transliteration: "hao", audio: nil),
                     Hanzi(context: context!, traditional: "不", simplified: "不", transliteration: "bu",  audio: nil) ]
        let content = CardContent(context: context!, traditional: "好不好", simplified: "好不好", pinyin: "hao bu hao", definitions: "", audio: nil)
        
        let card = Card(context: context!, questionType: .Text, answerType: .Text, dateAdded: NSDate(), timesViewed: 0, content: content, scoreData: CardScoreData(context: context!), deck: deck)
        saveContextAndWait(context!)
        
        let hanziArray = Hanzi.fetchUniqueHanziForCard(context!, card: card)
        
        XCTAssertTrue(hanziArray.isEmpty == false)
        XCTAssertTrue(hanziArray.count == 2)
    }
    
    func testFetchHanziArray() {
        let context = stack?.managedObjectContext
        
        let deck  = Deck(context: context!, name: "test", shortName: "test", deckDescription: "test", cards: NSMutableOrderedSet(), locked: false)
        let hanzi = [Hanzi(context: context!, traditional: "好", simplified: "好", transliteration: "hao", audio: nil),
                     Hanzi(context: context!, traditional: "不", simplified: "不", transliteration: "bu",  audio: nil) ]
        let content = CardContent(context: context!, traditional: "好不好", simplified: "好不好", pinyin: "hao bu hao", definitions: "", audio: nil)
        
        let card = Card(context: context!, questionType: .Text, answerType: .Text, dateAdded: NSDate(), timesViewed: 0, content: content, scoreData: CardScoreData(context: context!), deck: deck)
        
        saveContextAndWait(context!)
        
        let hanziArray = Card.fetchCardHanziArray(context!, card: card)
        
        XCTAssertTrue(hanziArray.isEmpty == false)
        XCTAssertTrue(hanziArray.count == 3)
        
        XCTAssertTrue(hanziArray[0].traditional == "好")
        XCTAssertTrue(hanziArray[1].traditional == "不")
        XCTAssertTrue(hanziArray[2].traditional == "好")
        XCTAssertTrue(hanziArray[2].objectID == hanziArray[0].objectID)
    }
    
    func testSaveAndFetchHanzi() {
        let context = stack?.managedObjectContext
        
        //  Save character 後/后
        let hanzi = Hanzi(context: context!, traditional: "後", simplified: "后", transliteration: "hòu", audio: nil)
        var saveResult: ContextSaveResult = saveContextAndWait(stack!.managedObjectContext)
        XCTAssertTrue(saveResult.success)
        
        //  Fetch by 後 -> Success
        let hou = Hanzi.fetch(context!, traditional: "後")
        XCTAssertTrue(hou != nil)
        
        if (hou == nil) {
            return
        }
        
        XCTAssertTrue(hou!.simplified == "后")
        XCTAssertTrue(hou!.transliteration == "hòu")
        XCTAssertTrue(hou!.audio == nil)
        
        //  Fetch by 后 -> Fail
        let simplifiedResults = Hanzi.fetch(context!, traditional: "后")
        XCTAssertTrue(simplifiedResults == nil)
        
        //  Update audio field
        hanzi.audio = NSData(base64EncodedString: "asdf", options: NSDataBase64DecodingOptions.IgnoreUnknownCharacters)
        saveResult = saveContextAndWait(stack!.managedObjectContext)
        XCTAssertTrue(saveResult.success)
        
        //  Fetch by 後 -> Success
        //  'hou' still points at the same managed object:
        let audioUpdateResults = Hanzi.fetch(context!, traditional: "後")
        XCTAssertTrue(audioUpdateResults != nil)
        XCTAssertTrue(hou!.simplified == "后")
        XCTAssertTrue(hou!.transliteration == "hòu")
        XCTAssertTrue(hou!.audio != nil)
    }
    
    func testImportAndConvertHSKDeck() {
        let context = stack?.managedObjectContext
        let deckJsonPath = NSBundle.mainBundle().pathForResource("hsk1", ofType: "json")
        let hskDeck = HSKDeck.loadDeckFromJsonFile(deckJsonPath!).deck!
        
        let decks = HSKImporter.convertHSKDecksToCoreData(context!, hskDecks: [hskDeck])
        XCTAssertTrue(decks.count == 1)
        
        let deck = decks[0]
        XCTAssertTrue(deck.cards.count == 150)
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock() {
            // Put the code you want to measure the time of here.
        }
    }

}
